import { Injectable } from '@angular/core';
import { Config } from '../config/main.config';
import { EventsService } from './events.service';
import { NavService } from './nav.service';
import { NetworkService } from './network.service';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  // endpoint: string = Config.api + "/user";
  _user: any;
  avatar: any;

  constructor(
    public utilityProvider: UtilityService,
    public sqlite: SqliteService,
    public events: EventsService,
    public network: NetworkService,
    public nav: NavService
  ) {}

  assignEvents() {
    this.events.subscribe('user:logout', this.logout.bind(this));
    this.events.subscribe('user:get', this.getUser.bind(this));
  }

  login(user) {
    return new Promise((resolve) => {
      console.log('user', user);

      const phone_data = {
        mobileNumber: user.mobileNumber,
        password: user.password,
        rememberMe: true,
      };

      this.network.login(phone_data).then(
        async (res) => {
          console.log('network', res);
          if (res && res.success === true) {
            let uuser = res.result.result;

            uuser['id'] = uuser._id;
            console.log(uuser);
            // this.menuCtrl.enable(true, 'authenticated');
            const token = res.result.token;
            uuser.token = token;
            localStorage.setItem('token', token);
            uuser.active = 1;
            await this.processUserData(uuser, false);
            resolve(true);
          } else resolve(false);
        },
        (err) => {
          console.log('network' + err);
          resolve(false);
        }
      );
    });
  }

  async LoginUser(res) {
    let uuser = res.result.finalData;
    uuser['id'] = uuser._id;
    console.log(uuser);
    // this.menuCtrl.enable(true, 'authenticated');
    const token = res.result.token;
    uuser.token = token;
    localStorage.setItem('token', token);
    uuser.active = 1;
    await this.processUserData(uuser, false);
  }

  logout() {
    // this.menuCtrl.enable(false, 'authenticated');
    // this.sqlite.setLogout();
    localStorage.removeItem('user');
    localStorage.removeItem('token');
    this.nav.setRoot('pages/login');
  }

  getLocalUser() {
    if (localStorage.getItem('user'))
      return JSON.parse(localStorage.getItem('user'));
  }

  setLocalUser(user){
    localStorage.setItem('user', JSON.stringify(user));
  }

  public getUser() {
    return new Promise(async (resolve) => {
      this.network.getUser().then(
        async (user: any) => {
          console.log('peel', user);
          user = user.user;
          if (user) {
            this.processUserData(user, false);
            resolve(user);
          } else {
            // redirect to steps
            this.logout();
          }
        },
        (err) => {
          this.logout();
        }
      );
    });
  }

  async processUserData(user, showelcome) {
    // check if sqlite set already, if not fetch records
    // const _user = user
    console.log('processUserData', user);

    // user.fcm_token = await this.firebaseService.getFCMToken();
    // this.user_role_id = parseInt(_user['role_id']);
    // this.utilityProvider.setKey('user_role_id', this.user_role_id);
    // const saveduser = await this.sqlite.setUserInDatabase(user);
    this.setLocalUser(user);

    // this.menuCtrl.enable(true, 'authenticated');

    // if (!saveduser) {
    //   this.logout();
    //   return;
    // }

    this.setUser(user);
    this.nav.setRoot('pages/home');
    // this.canBeResident = (parseInt(saveduser["can_user_become_resident"]) == 1);
    // this.canShowSettings = parseInt(saveduser["role_id"]) != 7

    // let currentUrl = this.nav.router.url;
    // console.log(currentUrl);

    // if (currentUrl == '/1/DashboardPage') {
    //   this.events.publish('dashboard:initialize');
    // } else {
    //   this.nav.setRoot('1/DashboardPage',
    //     {
    //       showelcome: showelcome,
    //       animate: true,
    //       direction: 'forward'
    //     }
    //   );
    // }
  }

  setUser(user) {
    this._user = user;
  }

  getCurrentUser() {
    return this._user;
  }

  update(data, token) {
    return {
      data,
      token,
    };
  }

  switchUserAccount(sw_user_id) {
    return new Promise<void>(async (resolve) => {
      await this.sqlite.switchLogin(sw_user_id);
      this.events.publish('stored:resetvariables');
      this.events.publish('user:get');
      resolve();
    });
  }
}

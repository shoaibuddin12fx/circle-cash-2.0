import { Injectable, Injector } from '@angular/core';
import { SQLite, SQLiteDatabaseConfig, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { Platform } from '@ionic/angular';
import { NetworkService } from './network.service';
import { UtilityService } from './utility.service';
import { StorageService } from './basic/storage.service';
import { browserDBInstance } from './browser-db-instance';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';


declare let window: any;
const SQL_DB_NAME = '__circlecash.db';

@Injectable({
  providedIn: 'root'
})
export class SqliteService {

  db: any;
  config: SQLiteDatabaseConfig = {
    name: '__circlecash.db',
    location: 'default'
  };

  msg = 'Sync In Progress ...';

  constructor(
    injector: Injector,
    public sqlite: SQLite,
    public platform: Platform,
    private storage: StorageService,
    private network: NetworkService,
    private utility: UtilityService,
    public diagnostic: Diagnostic,
    private androidpermissions: AndroidPermissions) {

  }

  initialize() {
    

    return new Promise(resolve => {

      this.storage.get('is_database_initialized').then(async v => {
        console.log(v);
        if (!v) {
          await this.initializeDatabase();
          resolve(true);
        } else {
          resolve(true);
        }
      });
    });

  }

  async initializeDatabase() {

    return new Promise(async resolve => {
      await this.platform.ready();
      // initialize database object
      await this.createDatabase();
      // initialize all tables

      // initialize users table
      await this.initializeUsersTable();
      // initialize users table

      this.storage.set('is_database_initialized', true);
      resolve(true);
    });


  }


  async initializeUsersTable() {

    return new Promise(resolve => {
      // create statement
      let sql = 'CREATE TABLE IF NOT EXISTS users(';
      sql += 'id TEXT PRIMARY KEY, ';
      sql += 'firstName TEXT, ';
      sql += 'lastName TEXT, ';
      sql += 'mobileNumber TEXT, ';
      // sql += 'profile_image TEXT, ';
      // sql += 'community TEXT, ';
      // sql += 'house TEXT, ';
      // sql += 'head_of_family INTEGER DEFAULT 0, ';
      // sql += 'can_manage_family INTEGER DEFAULT 0, ';
      // sql += 'can_send_passes INTEGER DEFAULT 0, ';
      // sql += 'can_retract_sent_passes INTEGER DEFAULT 0, ';
      // sql += 'fcm_token TEXT, ';
       sql += 'token TEXT, ';
      // sql += 'dial_code TEXT DEFAULT \'+1\', ';
      // sql += 'suspand INTEGER DEFAULT 0, ';
      // sql += 'allow_parental_control INTEGER DEFAULT 0, ';
      // sql += 'email_verification_code INTEGER DEFAULT 0, ';
      // sql += 'is_guard INTEGER DEFAULT 0, ';
      // sql += 'can_user_become_resident INTEGER DEFAULT 0, ';
      // sql += 'can_show_settings INTEGER DEFAULT 0, ';
      // sql += 'role_id INTEGER DEFAULT 0, ';
       sql += 'active INTEGER DEFAULT 0 )';
      // sql += 'is_reset_password INTEGER DEFAULT 0, ';
     // sql += 'licence_number TEXT )';

      this.msg = 'Initializing Users ...';
      resolve(this.execute(sql, []));
    });

  }


  async setUserInDatabase(user) {

    return new Promise(async resolve => {



      // check if user is already present in our local database, if not, create and fetch his data
      // check if user exist in database, if not create it else update it
      let sql = 'INSERT OR REPLACE INTO users(';
      sql += 'id, ';
      sql += 'firstName, ';
      sql += 'lastName, ';
      sql += 'mobileNumber ';
    //  sql += 'profile_image ';
      sql += ')';

      sql += 'VALUES (';

      sql += '?, ';
      sql += '?, ';
      sql += '?, ';
      sql += '? ';
    //  sql += '? ';
      sql += ')';

      const values = [
        user.id,
        user.firstName,
        user.lastName,
        user.mobileNumber,
     //   user.profile_image_url
      ];

      await this.execute(sql, values);

      console.log('checkToken', user.token);
      if (user.token) {

        const sql3 = 'UPDATE users SET active = ?';
        const values3 = [0];
        await this.execute(sql3, values3);

        const sql2 = 'UPDATE users SET token = ?, active = ? where id = ?';
        const values2 = [user.token, 1, user.id];

        await this.execute(sql2, values2);

      }

      resolve(await this.getActiveUser());

    });
  }

  public async getActiveUserId() {

    return new Promise(async resolve => {

      let user = JSON.parse(localStorage.getItem('user'));
      
      if(user){
        resolve(user._id)
      }else{
        resolve(null)
      }
      
      return;
      const sql = 'SELECT id FROM users where active = ?';
      const values = [1];

      const d = await this.execute(sql, values);
      console.log(d);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        const id = data[0].id;
        resolve(id);
      } else {
        resolve(null);
      }

    });

  }

  public async getActiveUser() {
    return new Promise(async resolve => {

      let user = JSON.parse(localStorage.getItem('user'));
      
      if(user){
        resolve(user)
      }else{
        resolve(null)
      }
      
      return;

      const sql = 'SELECT * FROM users where active = ?';
      const values = [1];

      const d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        const user = data[0];
        resolve(user);
      } else {
        resolve(null);
      }

    });
  }



  public async getCurrentUserAuthorizationToken() {
    return new Promise(async resolve => {
      const user_id = await this.getActiveUserId();
      const sql = 'SELECT token FROM users where id = ? limit 1';
      const values = [user_id];

      const d = await this.execute(sql, values);
      // this.utility.presentToast(d);
      if (!d) {
        resolve(null);
        return;
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data[0].token);
      } else {
        resolve(null);
      }

    });
  }

  removeLoginFromLocal(user_id) {

    return new Promise(async resolve => {
      const sql = 'DELETE FROM users where id = ?';
      const values = [user_id];
      await this.execute(sql, values);
      const users = await this.getAllRecords('users');
      resolve(users);
    });

  }


  setLogout() {

    return new Promise(async resolve => {
      const user_id = await this.getActiveUserId();

      const sql = 'UPDATE users SET token = ?, active = ? where id = ?';
      const values = [null, 0, user_id];

      const d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(true);
      } else {
        resolve(false);
      }


    });

  }

  switchLogin(user_id) {

    return new Promise<void>(async (resolve) => {

      const sql3 = 'UPDATE users SET active = ?';
      const values3 = [0];
      await this.execute(sql3, values3);

      const sql2 = 'UPDATE users SET active = ? where id = ?';
      const values2 = [1, user_id];

      await this.execute(sql2, values2);

      resolve();

    });

  }


  getModelCount(table, query, array) {

    // sql starts from - where
    return new Promise(async resolve => {
      const sql = `SELECT COUNT(*) FROM ${table} ${query}`;
      const values = array;

      const d = await this.execute(sql, values);
      if (!d) { resolve(0); return; }
      const data = this.getRows(d);
      if (data.length == 0) { resolve(0); return; }
      resolve(data[0]['COUNT(*)']);
    });

  }

  execute(sql, params) {

    return new Promise(async resolve => {
      
      await this.platform.ready();
      if (!this.db) {
        // initialize database object
        await this.createDatabase();
      }

      console.log(sql);
      // // if(this.platform.is('cordova')){
      console.log(params);
      this.db.executeSql(sql, params).then(response => {
        resolve(response);
      }).catch(err => {
        console.error(err);
        resolve(null);
      });

    });
  }

  prepareBatch(insertRows) {

    return new Promise(async resolve => {

      const size = 250; const arrayOfArrays = [];

      for (let i = 0; i < insertRows.length; i += size) {
        arrayOfArrays.push(insertRows.slice(i, i + size));
      }

      console.log(arrayOfArrays);

      for (const element of arrayOfArrays) {
        await this.executeBatch(element);
        // await this.execute(s, p)
      }

      resolve(true);

    });
  }

  executeBatch(array) {

    return new Promise(async resolve => {

      if (!this.db) {
        await this.platform.ready();
        // initialize database object
        await this.createDatabase();
      }

      console.log(array);
      this.db.sqlBatch(array).then(response => {
        resolve(response);
      }).catch(err => {
        console.error(err);
        resolve(null);
      });

    });
  }

  async createDatabase() {

    var self = this;
    return new Promise(async resolve => {
      
      if (this.platform.is('cordova')) {
        
        
        const flag = await this.checkPermisssions();
        if(!flag){
          alert("Please allow storage access for application")
          resolve(false);
        }else{
          self.db = await self.sqlite.create(self.config);
          self.msg = 'Database initialized';
          console.log(self.msg);
          resolve(true);  
        }
        // this.androidpermissions.checkPermission(this.androidpermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(async result =>{
        //   if(result.hasPermission){
        //     self.db = await self.sqlite.create(self.config);
        //     self.msg = 'Database initialized';
        //     console.log(self.msg);
        //     resolve(true);
        //   }

        //   else{
        //     self.androidpermissions.requestPermission(self.androidpermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(async (result)=>{
        //       if(result.hasPermission){
        //         self.db = await self.sqlite.create(self.config);
        //         self.msg = 'Database initialized';
        //         console.log(self.msg);
        //         resolve(true);
        //       }else{
        //         alert("Please allow storage access for application")
        //         resolve(false);
        //       }
        //     })
        //   }
        // })

        
      } else {
        const db = window.openDatabase(SQL_DB_NAME, '1.0', 'DEV', 5 * 1024 * 1024);
        this.db = browserDBInstance(db);
        this.msg = 'Database initialized';
        resolve(true);
      }
      
    });


  }

  checkPermisssions(){
    var self = this;
    return new Promise(async resolve => {

      this.diagnostic.requestExternalStorageAuthorization().then(()=>{
        //User gave permission 
        resolve(true);
      }).catch(error=>{
        //Handle error
        resolve(false);
      });
      //  this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(async result =>{
      //     if(result.hasPermission){            
      //       resolve(true);
      //     }else{
      //       self.androidPermissions.requestPermission(self.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(async (result)=>{
      //         if(result.hasPermission){
      //           resolve(true);
      //         }else{
      //           alert("Please allow storage access for application")
      //           resolve(false);
      //         }
      //       })
      //     }
      //   })
      });
  }

  getRows(data) {
    const items = [];
    for (let i = 0; i < data.rows.length; i++) {
      const item = data.rows.item(i);

      items.push(item);
    }

    return items;
  }

  async getAllRecords(table) {

    return new Promise(async resolve => {
      console.log(table);
      const sql = 'SELECT * FROM ' + table;
      const values = [];

      const d = await this.execute(sql, values);

      if (!d) {
        resolve([]);
        return;
      }

      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }

    });
  }



}

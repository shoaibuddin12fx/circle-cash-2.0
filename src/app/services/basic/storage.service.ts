import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  // nativeStorage = localStorage;
  constructor(/*private nativeStorage: NativeStorage*/) { }

  set(key, data): Promise<boolean>{

    return new Promise( resolve => {

      localStorage.setItem(key, JSON.stringify(data))
      resolve(true);
      // .then(
      //   () => resolve(true),
      //   error => resolve(false)
      // );
    });

  }

  get(key): Promise<any>{

    return new Promise( resolve => {
      let data = localStorage.getItem(key);
      resolve(data)
      // .then(
      //   data => { console.log("getKey", data);  resolve(data) },
      //   error => resolve(null)
      // );
    });

  }

}

import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class StringsService {

  constructor() { }

  getOnlyDigits(phoneNumber) {
    var numberString = phoneNumber;
    var numberInDigits = numberString.replace(/[^\d]/g,'')
    var numberVal = parseInt(numberInDigits);
    console.log(numberVal);
    return numberVal.toString();
  }

  isPhoneNumberValid(number) {
    var _validPhoneNumber = this.getOnlyDigits(
      number
    );
    // remove trailing zeros
    let s = _validPhoneNumber.toString()
    return (_validPhoneNumber.toString().length < 10) ? false : true;
  }

  capitalizeEachFirst(str: string) {
    if(!str) return '';
    var splitStr = str.toLowerCase().split(' ');
    for (var i = 0; i < splitStr.length; i++) {
      // You do not need to check if i is larger than splitStr length, as your for does that for you
      // Assign it back to the array
      splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    // Directly return the joined string
    return splitStr.join(' ');
  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true })
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  parseAddressFromProfile(__profile) {
    return `${__profile["apartment"] || ""} ${__profile["street_address"] || ""} ${__profile["city"] || ""} ${__profile["state"] || ""} ${__profile["zip_code"] || ""}`;
  }

  parseName(input) {

    const capitalize = (s) => {
      if (typeof s !== 'string') return ''
      s = s.toLowerCase()
      return s.charAt(0).toUpperCase() + s.slice(1)
    }

    var fullName = input || "";
    var result = {};

    if (fullName.length > 0) {
      var nameTokens = fullName.match(/\w*/g) || [];
      nameTokens = nameTokens.filter(n => n)

      if (nameTokens.length > 3) {
        result['firstName'] = nameTokens.slice(0, 2).join(' ');
        result['firstName'] = capitalize(result['firstName']);
      } else {
        result['firstName'] = nameTokens.slice(0, 1).join(' ');
        result['firstName'] = capitalize(result['firstName']);
      }

      if (nameTokens.length > 2) {
        result['middleName'] = nameTokens.slice(-2, -1).join(' ');
        result['lastName'] = nameTokens.slice(-1).join(' ');
        result['middleName'] = capitalize(result['middleName']);
        result['lastName'] = capitalize(result['lastName']);
      } else {
        if (nameTokens.length == 1) {
          result['lastName'] = "";
          result['middleName'] = "";
        } else {
          result['lastName'] = nameTokens.slice(-1).join(' ');
          result['lastName'] = capitalize(result['lastName']);
          result['middleName'] = "";
        }

      }
    }

    var display_name = result['lastName'] + (result['lastName'] ? ' ' : '') + result['firstName'];
    return display_name;
  }

  isLastNameExist(input) {
    var fullname = this.parseName(input);
    return !(fullname['lastName'] == "")
  }

  formatPhoneNumberRuntime(phoneNumber) {

    if (phoneNumber == null || phoneNumber == "") { return phoneNumber }
    var cleaned = ("" + phoneNumber).replace(/\D/g, '');
    function numDigits(x) {
      return Math.log(x) * Math.LOG10E + 1 | 0;
    }

    // only keep number and +
    var p1 = cleaned.match(/\d+/g);
    if (p1 == null) { return cleaned }
    var p2 = phoneNumber.match(/\d+/g).map(Number);
    var len = numDigits(p2)
    // document.write(len + " " );
    switch (len) {
      case 1:
      case 2:
        return "(" + phoneNumber.toString()
      case 3:
        return "(" + phoneNumber.toString() + ")"
      case 4:
      case 5:
      case 6:
        var f1 = "(" + phoneNumber.toString().substring(0, 3) + ")"
        var f2 = phoneNumber.toString().substring(len, 3)
        return f1 + " " + f2
      case 7:
      case 8:
      case 9:
      case 10:
        f1 = "(" + phoneNumber.toString().substring(0, 3) + ")"
        f2 = phoneNumber.toString().substring(6, 3)
        var f3 = phoneNumber.toString().substring(len + 1, 6)
        return f1 + " " + f2 + "-" + f3
      default:
        phoneNumber = phoneNumber.replace(/\D/g, '').substr(phoneNumber.length - 10);
        f1 = "(" + phoneNumber.toString().substring(0, 3) + ")"
        f2 = phoneNumber.toString().substring(6, 3)
        var f3 = phoneNumber.toString().substring(len, 4)
        return f1 + " " + f2 + "-" + f3

    }


    // return "len";

  }

  onkeyupFormatPhoneNumberRuntime(phoneNumber, last = true) {
    
    if (phoneNumber == null || phoneNumber == '') { return phoneNumber; }

    phoneNumber = this.getOnlyDigits(phoneNumber);
    // phoneNumber = phoneNumber.substring(phoneNumber.length - 1,-11);//keep only 10 digit Number
    // phoneNumber = phoneNumber.substring(phoneNumber.length - 10, -11);//keep only 10 digit Number
    phoneNumber = last ? phoneNumber.substring(phoneNumber.length - 10, phoneNumber.length) : phoneNumber.substring(0, 10);

    const cleaned = ('' + phoneNumber).replace(/\D/g, '');

    function numDigits(x: number) {
      return Math.log(x) * Math.LOG10E + 1 | 0;
    }

    // only keep number and +
    const p1 = cleaned.match(/\d+/g);
    if (p1 == null) { return cleaned; }
    const p2 = phoneNumber.match(/\d+/g).map(Number);
    const len = numDigits(p2);
    // document.write(len + " " );
    return phoneNumber;
    switch (len) {
      case 1:
      case 2:
        return '(' + phoneNumber;
      case 3:
        return '(' + phoneNumber + ')';
      case 4:
      case 5:
      case 6:
        var f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
        var f2 = phoneNumber.toString().substring(len, 3);
        return f1 + ' ' + f2;
      default:
        f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
        f2 = phoneNumber.toString().substring(3, 6);
        var f3 = phoneNumber.toString().substring(6, 10);

        console.log(phoneNumber, f3);
        return f1 + ' ' + f2 + '-' + f3;
    }
  }

  onkeyupFormatCardNumberRuntime(cardNumber, last = true){
    if (cardNumber == null || cardNumber == '') { return cardNumber; }

    cardNumber = this.getOnlyDigits(cardNumber);
    // cardNumber = cardNumber.substring(cardNumber.length - 1,-11);//keep only 10 digit Number
    // cardNumber = cardNumber.substring(cardNumber.length - 10, -11);//keep only 10 digit Number
    cardNumber = last ? cardNumber.substring(cardNumber.length - 16, cardNumber.length) : cardNumber.substring(0, 16);

    const cleaned = ('' + cardNumber).replace(/\D/g, '');

    function numDigits(x: number) {
      return Math.log(x) * Math.LOG10E + 1 | 0;
    }



    // only keep number and +
    const p1 = cleaned.match(/\d+/g);
    if (p1 == null) { return cleaned; }
    const p2 = cardNumber.match(/\d+/g).map(Number);
   // const len = numDigits(p2);
    console.log(cardNumber);
    var temp = "";
    var value = cardNumber;

    var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '')
    var matches = v.match(/\d{4,16}/g);
    var match = matches && matches[0] || ''
    var parts = []

    for (var i=0, len=match.length; i<len; i+=4) {
        parts.push(match.substring(i, i+4))
    }

    if (parts.length) {
        return parts.join(' ')
    } else {
        return value
    }

// if(cardNumber.length === 5){
//   cardNumber = cardNumber.toString().substring(0, 4) + '-' + cardNumber.toString().substring(4, 5);
//   console.log(cardNumber)
// }
// else if(cardNumber.length === 10) {
//    let _previous = cardNumber.toString().substring(0,9)
//    let result = _previous + '-' + cardNumber.toString().substring(9, 10)
//    console.log(result)
// } else if(cardNumber.length === 15) {
//    let _previous = cardNumber.toString().substring(0, 14)
//    let result = _previous + '-' + cardNumber.toString().substring(14, 15)
//    console.log(result)
// } 
 
// console.log(cardNumber.length)

    // switch (len) {
    //   case 1:
    //     return cardNumber;
    //   case 2:
    //     return cardNumber;
    //   case 3:
    //     return cardNumber;
    //   case 4:
    //     return cardNumber;
    //   case 5:   
    //     return cardNumber.toString().substring(0, 4) + '-' + cardNumber.toString().substring(5, );
    //   case 6:
    //   case 7:
    //   case 8:
    //   case 9:    
    //     return cardNumber.toString().substring(0, 4) + '-' + cardNumber.toString().substring(5, 9) ;
      // case 4:
      // case 5:
      // case 6:
      //   var f1 = '(' + cardNumber.toString().substring(0, 3) + ')';
      //   var f2 = cardNumber.toString().substring(len, 3);
      //   return f1 + ' ' + f2;
      // default:
      //   f1 = '(' + cardNumber.toString().substring(0, 3) + ')';
      //   f2 = cardNumber.toString().substring(3, 6);
      //   var f3 = cardNumber.toString().substring(6, 10);

      //   console.log(cardNumber, f3);
      //   return f1 + ' ' + f2 + '-' + f3;
   // }

    return cardNumber;
    // document.write(len + " " );
    
  }




}

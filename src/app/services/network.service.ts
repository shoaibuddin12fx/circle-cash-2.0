import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { EventsService } from './events.service';
import { ApiService } from './api.service';
import { UtilityService } from './utility.service';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class NetworkService {
  constructor(
    public utility: UtilityService,
    public api: ApiService,
    private events: EventsService
  ) {
    // console.log('Hello NetworkProvider Provider');
  }

  // post requests -- start

  login(data) {
    return this.httpPostResponse('user/loginCustomer', data, null, true);
  }

  register(data) {
    return this.httpPostResponse('user/signUpCircleCash', data, null, true);
  }

  rechargeElectricityMeter(data) {
    return this.httpPostResponse(
      'bill/prepaidElectricityBillCircleCash',
      data,
      null,
      true
    );
  }

  topupMobile(data) {
    return this.httpPostResponse(
      'bill/topupMobileCircleCash',
      data,
      null,
      true
    );
  }

  addEducationPayment(data) {
    return this.httpPostResponse(
      'bill/addEducationPaymentCircleCash',
      data,
      null,
      true
    );
  }

  addMobileAccount(data) {
    return this.httpPostResponse('bill/addMobileAccount', data, null, true);
  }

  getMobileAccounts(data) {
    return this.httpPostResponse('bill/getMobileAccounts', data, null, true);
  }

  getMeterAccounts(data) {
    return this.httpPostResponse('bill/getMeterAccounts', data, null, true);
  }

  addMeterAccount(data) {
    return this.httpPostResponse('bill/addMeterAccount', data, null, true);
  }

  addEducationPayment2(data) {
    return this.httpPostResponse(
      'bill/addEducationPaymentCircleCash2',
      data,
      null,
      true
    );
  }

  addCard(data) {
    return this.httpPostResponse('user/addCard', data, null, true);
  }

  updateCard(data) {
    return this.httpPostResponse('user/updateCard', data, null, true);
  }

  getCards(data) {
    return this.httpPostResponse('user/getCards', data, null, true);
  }

  addBankAccount(data) {
    return this.httpPostResponse('user/addBankAccount', data, null, true);
  }

  getBankAccounts(data) {
    return this.httpPostResponse('user/getBankAccounts', data, null, true);
  }

  addCardTransfer(data) {
    return this.httpPostResponse('user/addCardTransfer', data, null, true);
  }

  generateVoucher(data) {
    return this.httpPostResponse('user/generateVoucher', data, null, true);
  }

  getUser() {
    return this.httpPostResponse('user/getProfile', null, null, true);
  }

  payE15Service(data) {
    return this.httpPostResponse('user/payE15Services', data, null, true);
  }

  InquiryE15Services(data) {
    return this.httpPostResponse('user/InquiryE15Services', data, null, true);
  }

  payCustomsServices(data) {
    return this.httpPostResponse('user/payCustomsServices', data, null, true);
  }

  inquiryCustomsServices(data) {
    return this.httpPostResponse(
      'user/inquiryCustomsServices',
      data,
      null,
      true
    );
  }

  verifyMobileNumber(data) {
    return this.httpPostResponse('user/verifyMobileNumber', data, null, true);
  }

  resendOTP(data) {
    return this.httpPostResponse('user/resendOTP', data, null, true);
  }

  getCircleCashUser(data) {
    return this.httpPostResponse('user/getCircleCashUser', data, null, true);
  }

  addLocalCashCircleAccount(data) {
    return this.httpPostResponse(
      'user/addLocalCashCircleAccount',
      data,
      null,
      true
    );
  }

  getCircleCashContactList(data) {
    return this.httpPostResponse(
      'user/getCircleCashContactList',
      data,
      null,
      true
    );
  }

  addLocalBankAccount(data) {
    return this.httpPostResponse('user/addLocalBankAccount', data, null, true);
  }

  transactionHistoryOfCustomer(data) {
    return this.httpGetResponse(
      'user/transactionHistoryOfCustomer',
      data,
      true,
      true
    );
  }

  forgotPasswordCircleCash(data) {
    return this.httpPostResponse(
      'user/forgotPasswordCircleCash',
      data,
      null,
      true
    );
  }

  changePasswordVerified(data) {
    return this.httpPostResponse(
      'user/changePasswordVerified',
      data,
      null,
      true
    );
  }

  changePassword(data) {
    return this.httpPostResponse('user/changePassword', data, null, true);
  }

  getProfile(data) {
    return this.httpGetResponse('user/getProfile', data, true, true);
  }

  editsettingInformation(data) {
    return this.httpPostResponse(
      'user/editsettingInformation',
      data,
      null,
      true
    );
  }

  verifyOtp(data) {
    return this.httpPostResponse('user/verifyOtp', data, null, true);
  }

  accountTransfer(data) {
    return this.httpPostResponse('user/accountTransfer', data, null, true);
  }

  getQRCodeResult(data) {
    return this.httpPostResponse('user/getQRCodeResult', data, null, true);
  }

  deleteCard(data) {
    return this.httpPostResponse('user/deleteCard', data, null, true);
  }

  savePin(data) {
    return this.httpPostResponse('user/savePin', data, null, true);
  }

  testCall(data) {
    return this.httpPostResponse('test/testCallToMiddleware', data, null, true);
  }

  // get requests -- end

  httpPostResponse(
    key,
    data,
    id = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json'
  ) {
    return this.httpResponse(
      'post',
      key,
      data,
      id,
      showloader,
      showError,
      contenttype
    );
  }

  httpGetResponse(
    key,
    id = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json'
  ) {
    return this.httpResponse(
      'get',
      key,
      {},
      id,
      showloader,
      showError,
      contenttype
    );
  }

  // default 'Content-Type': 'application/json',
  httpResponse(
    type = 'get',
    key,
    data,
    query = null,
    showloader = false,
    showError = true,
    contenttype = 'application/json'
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      if (showloader == true) {
        this.utility.showLoader();
      }

      //const _id = (id) ? '/' + id : '';
      var url = key;
      if (query) url = key + query;

      let reqOpts = {
        headers: new HttpHeaders({
          'Content-Type': contenttype,
          language: 'en',
        }),
      };
      console.log(url, data, reqOpts);

      const seq =
        type == 'get'
          ? this.api.get(url, null, reqOpts)
          : this.api.post(url, data, reqOpts);

      seq.subscribe(
        (res: any) => {
          console.log('showloader ?', showloader);
          if (showloader == true) {
            this.utility.hideLoader();
          }
          console.log('response_code', res.response_code);
          if (res.response_code != 200) {
            this.utility.presentFailureToast(res.response_message);
            resolve({ success: false, result: res });
          } else {
            resolve({ success: true, result: res.result });
          }
          // this.utility.presentSuccessToast(res['message']);
        },
        (err) => {
          console.log('Error in API', err);
          let error = err['error'];
          if (showloader == true) {
            this.utility.hideLoader();
          }

          if (showError) {
            this.utility.presentFailureToast(error['message']);
          }

          console.log('Error in API', err);

          reject(null);
        }
      );
    });
  }

  showFailure(err) {
    console.error('ERROR', err);
    var _error = err ? err['response_message'] : 'check logs';
    this.utility.presentFailureToast(_error);
  }

  getActivityLogs() {
    return this.httpGetResponse('get_activity_logs', null, true);
  }

  getPermissions() {
    return this.httpGetResponse('get-all-permissions', null, false);
  }

  getChatList(loader) {
    return this.httpGetResponse('online-offline?update_for_me=1', null, loader);
  }

  getChatHistory(id) {
    return this.httpGetResponse('chat/history', id, true);
  }

  sendMessage(param) {
    return this.httpPostResponse(
      'chat/message-post',
      param,
      null,
      false,
      false
    );
  }

  markAsRead(id) {
    return this.httpPostResponse('chat/read', {}, id, false, false);
  }

  async updateNotesProject(data, id) {
    return this.httpPostResponse(
      'update-project-notes',
      data,
      id,
      false,
      false
    );
  }

  createInvoice(data, id) {
    return this.httpPostResponse('create-invoice', data, id, true, false);
  }

  getInvoice(projectId, invoiceId) {
    return this.httpGetResponse(
      `show-invoice/${projectId}/${invoiceId}`,
      null,
      true,
      false
    );
  }

  updateInvoice(data, invoiceId) {
    return this.httpPostResponse(
      'update-invoice',
      data,
      invoiceId,
      true,
      false
    );
  }

  async deleteInvoiceProject(param, id) {
    return this.httpPostResponse('delete-invoice', param, id, false, false);
  }

  uploadContract(data) {
    return this.httpPostResponse(
      'contract-upload-mobile',
      data,
      null,
      true,
      false
    );
  }

  generateInvoiceLink(id) {
    return this.httpGetResponse('generate-invoice-link', id, true, false);
  }

  generateContractLink(id) {
    return this.httpGetResponse('generate-contract-link', id, true, false);
  }

  async updateBio(data) {
    return this.httpPostResponse('update_profile', data, null, true, false);
  }

  async getTaskSkills(id) {
    return this.httpGetResponse('get-task-skills', id, false, false);
  }

  saveFeedback(data) {
    this.httpPostResponse('add-supervisor-task-notes', data, null, true);
  }

  createContract(projectId, data) {
    this.httpPostResponse('create-contract', data, projectId, true);
  }

  getSkills() {
    return this.httpGetResponse('get-skills-multiselect', null, false, false);
  }

  async removePhaseMember(payload) {
    await this.httpPostResponse('delete-phase-member', payload, null, true);
  }

  async getPhaseline(id) {
    return this.httpGetResponse('get-phase', id, true, false);
  }
}

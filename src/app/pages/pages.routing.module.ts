import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'splash', 
    pathMatch: 'full' 
  },
  {
    path: 'splash',
    loadChildren: () => import('./splash/splash.module').then( m => m.SplashPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'otp-verify',
    loadChildren: () => import('./otp-verify/otp-verify.module').then( m => m.OtpVerifyPageModule)
  },
  { 
    path: 'account-created',
    loadChildren: () => import('./account-created/account-created.module').then( m => m.AccountCreatedPageModule)
  },
  {
    path: 'my-qr-code',
    loadChildren: () => import('./my-qr-code/my-qr-code.module').then( m => m.MyQrCodePageModule)
  },
  {
    path: 'qr-code',
    loadChildren: () => import('./qr-code/qr-code.module').then( m => m.QrCodePageModule)
  },
  {
    path: 'scan-qr-code',
    loadChildren: () => import('./scan-qr-code/scan-qr-code.module').then( m => m.ScanQrCodePageModule)
  },
  {
    path: 'payments-form-card',
    loadChildren: () => import('./payments-form-card/payments-form-card.module').then( m => m.PaymentsFormCardPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'generate-pin',
    loadChildren: () => import('./generate-pin/generate-pin.module').then( m => m.GeneratePinPageModule)
  },
  {
    path: 'verify-account',
    loadChildren: () => import('./verify-account/verify-account.module').then( m => m.VerifyAccountPageModule)
  },
  {
    path: 'enter-pin',
    loadChildren: () => import('./enter-pin/enter-pin.module').then( m => m.EnterPinPageModule)
  },
  {
    path: 'prepaid-electricity-bill',
    loadChildren: () => import('./prepaid-electricity-bill/prepaid-electricity-bill.module').then( m => m.PrepaidElectricityBillPageModule)
  },
  {
    path: 'prepaid-electricity-bill-confirm',
    loadChildren: () => import('./prepaid-electricity-bill-confirm/prepaid-electricity-bill-confirm.module').then( m => m.PrepaidElectricityBillConfirmPageModule)
  },
  {
    path: 'telecom-services',
    loadChildren: () => import('./telecom-services/telecom-services.module').then( m => m.TelecomServicesPageModule)
  },
  {
    path: 'government-services',
    loadChildren: () => import('./government-services/government-services.module').then( m => m.GovernmentServicesPageModule)
  },
  {
    path: 'e15-services',
    loadChildren: () => import('./e15-services/e15-services.module').then( m => m.E15ServicesPageModule)
  },
  {
    path: 'customs-services',
    loadChildren: () => import('./customs-services/customs-services.module').then( m => m.CustomsServicesPageModule)
  },
  {
    path: 'e-invoices',
    loadChildren: () => import('./e-invoices/e-invoices.module').then( m => m.EInvoicesPageModule)
  },
  {
    path: 'e-invoice',
    loadChildren: () => import('./e-invoice/e-invoice.module').then( m => m.EInvoicePageModule)
  },
  {
    path: 'add-payee',
    loadChildren: () => import('./add-payee/add-payee.module').then( m => m.AddPayeePageModule)
  },
  {
    path: 'type-selection',
    loadChildren: () => import('./type-selection/type-selection.module').then( m => m.TypeSelectionPageModule)
  },
  {
    path: 'electricity-purchase',
    loadChildren: () => import('./electricity-purchase/electricity-purchase.module').then( m => m.ElectricityPurchasePageModule)
  },
  {
    path: 'telecom-service-providers',
    loadChildren: () => import('./telecom-service-providers/telecom-service-providers.module').then( m => m.TelecomServiceProvidersPageModule)
  },
  {
    path: 'add-education-service',
    loadChildren: () => import('./add-education-service/add-education-service.module').then( m => m.AddEducationServicePageModule)
  },
  {
    path: 'add-local-cash-circle-account',
    loadChildren: () => import('./add-local-cash-circle-account/add-local-cash-circle-account.module').then( m => m.AddLocalCashCircleAccountPageModule)
  },
  {
    path: 'local-cash-circle-account',
    loadChildren: () => import('./local-cash-circle-account/local-cash-circle-account.module').then( m => m.LocalCashCircleAccountPageModule)
  },
  {
    path: 'card-transfer',
    loadChildren: () => import('./card-transfer/card-transfer.module').then( m => m.CardTransferPageModule)
  },
  {
    path: 'card-transfer-details',
    loadChildren: () => import('./card-transfer-details/card-transfer-details.module').then( m => m.CardTransferDetailsPageModule)
  },
  {
    path: 'generate-voucher',
    loadChildren: () => import('./generate-voucher/generate-voucher.module').then( m => m.GenerateVoucherPageModule)
  },
  {
    path: 'generate-voucher-details',
    loadChildren: () => import('./generate-voucher-details/generate-voucher-details.module').then( m => m.GenerateVoucherDetailsPageModule)
  },
  {
    path: 'credit-debit-cards',
    loadChildren: () => import('./credit-debit-cards/credit-debit-cards.module').then( m => m.CreditDebitCardsPageModule)
  },
  {
    path: 'add-credit-debit-card',
    loadChildren: () => import('./add-credit-debit-card/add-credit-debit-card.module').then( m => m.AddCreditDebitCardPageModule)
  },
  {
    path: 'cards-and-details',
    loadChildren: () => import('./cards-and-details/cards-and-details.module').then( m => m.CardsAndDetailsPageModule)
  },
  {
    path: 'notifications',
    loadChildren: () => import('./notifications/notifications.module').then( m => m.NotificationsPageModule)
  },
  {
    path: 'user-profile',
    loadChildren: () => import('./user-profile/user-profile.module').then( m => m.UserProfilePageModule)
  },
  {
    path: 'update-password',
    loadChildren: () => import('./update-password/update-password.module').then( m => m.UpdatePasswordPageModule)
  },
  {
    path: 'delete-card',
    loadChildren: () => import('./delete-card/delete-card.module').then( m => m.DeleteCardPageModule)
  },
  {
    path: 'add-local-bank-account',
    loadChildren: () => import('./add-local-bank-account/add-local-bank-account.module').then( m => m.AddLocalBankAccountPageModule)
  },
  {
    path: 'add-local-bank-account-confirmation',
    loadChildren: () => import('./add-local-bank-account-confirmation/add-local-bank-account-confirmation.module').then( m => m.AddLocalBankAccountConfirmationPageModule)
  },
  {
    path: 'payee-list',
    loadChildren: () => import('./payee-list/payee-list.module').then( m => m.PayeeListPageModule)
  },
  {
    path: 'card-transfer-list',
    loadChildren: () => import('./card-transfer-list/card-transfer-list.module').then( m => m.CardTransferListPageModule)
  },
  {
    path: 'cash-circle-contact-list',
    loadChildren: () => import('./cash-circle-contact-list/cash-circle-contact-list.module').then( m => m.CashCircleContactListPageModule)
  },
  {
    path: 'edit-payee-detail',
    loadChildren: () => import('./edit-payee-detail/edit-payee-detail.module').then( m => m.EditPayeeDetailPageModule)
  },
  {
    path: 'education-bills-and-fees',
    loadChildren: () => import('./education-bills-and-fees/education-bills-and-fees.module').then( m => m.EducationBillsAndFeesPageModule)
  },
  {
    path: 'education-service',
    loadChildren: () => import('./education-service/education-service.module').then( m => m.EducationServicePageModule)
  },
  {
    path: 'identity-verified',
    loadChildren: () => import('./identity-verified/identity-verified.module').then( m => m.IdentityVerifiedPageModule)
  },
  {
    path: 'new-password-generated',
    loadChildren: () => import('./new-password-generated/new-password-generated.module').then( m => m.NewPasswordGeneratedPageModule)
  },
  {
    path: 'successful-payment-qr',
    loadChildren: () => import('./successful-payment-qr/successful-payment-qr.module').then( m => m.SuccessfulPaymentQrPageModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}

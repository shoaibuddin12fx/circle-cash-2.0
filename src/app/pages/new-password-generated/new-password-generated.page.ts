import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-new-password-generated',
  templateUrl: './new-password-generated.page.html',
  styleUrls: ['./new-password-generated.page.scss'],
})
export class NewPasswordGeneratedPage extends BasePage implements OnInit {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }
  
  navigate(path){
    this.nav.navigateTo(path);
  }
}

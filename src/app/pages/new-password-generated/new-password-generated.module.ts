import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewPasswordGeneratedPageRoutingModule } from './new-password-generated-routing.module';

import { NewPasswordGeneratedPage } from './new-password-generated.page';
import { HeaderComponent } from 'src/app/components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewPasswordGeneratedPageRoutingModule
  ],
  declarations: [NewPasswordGeneratedPage, HeaderComponent]
})
export class NewPasswordGeneratedPageModule {}

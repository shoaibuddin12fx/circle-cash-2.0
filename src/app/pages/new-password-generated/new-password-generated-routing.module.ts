import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewPasswordGeneratedPage } from './new-password-generated.page';

const routes: Routes = [
  {
    path: '',
    component: NewPasswordGeneratedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewPasswordGeneratedPageRoutingModule {}

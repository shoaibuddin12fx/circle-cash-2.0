import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-verify-account',
  templateUrl: './verify-account.page.html',
  styleUrls: ['./verify-account.page.scss'],
})
export class VerifyAccountPage extends BasePage implements OnInit {
  OTP: string = '';
  mobileNumber;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    let params = this.nav.getQueryParams();
    this.OTP = params.otp;
    this.mobileNumber = params.mobileNumber;
  }

  navigate(path){
    this.nav.navigateTo(path)
  }

  otpController(event,next,prev, index){

    if(index == 6) {
      console.log("submit")
    }
    if(event.target.value.length < 1 && prev){
      prev.setFocus()
    }
    else if(next && event.target.value.length>0){
      next.setFocus();
    }
    else {
     return 0;
    }
    console.log("OTP" ,this.OTP) 
 }

 async onOTPVerify() {
   if(this.OTP && this.OTP.length == 4){
     let obj = {otp:this.OTP, mobileNumber: this.mobileNumber};
     let result = await this.network.verifyOtp(obj);
     if(result && result.success === true){
       this.nav.push('pages/identity-verified',{mobileNumber: obj.mobileNumber});
     }
    // this.network.verify('')
   }
 }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { E15ServicesPage } from './e15-services.page';

const routes: Routes = [
  {
    path: '',
    component: E15ServicesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class E15ServicesPageRoutingModule {}

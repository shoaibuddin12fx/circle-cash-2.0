import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-e15-services',
  templateUrl: './e15-services.page.html',
  styleUrls: ['./e15-services.page.scss'],
})
export class E15ServicesPage extends BasePage implements OnInit {
  isInquiry: boolean = false;
  inquiryForm: FormGroup;
  payForm:FormGroup;
  cardSelected = true;
  inquirySubmitted = false;
  paySubmitted = false;
  mobileNumbers = [];
  cards = [];

  constructor(injector:Injector) {super(injector)}
  
  ngOnInit() 
  {
    this.setupForms();
    this.getMobileNumbers();
  }
 
  OnCardSelected(selected){
    this.cardSelected = selected;
  }

  setupForms(){
    this.inquiryForm = this.formBuilder.group({
      invoiceNumber: ['', Validators.compose([Validators.required])],
      mobileId: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
      cardNumber: ['', Validators.compose([Validators.required])]
    })

    this.payForm = this.formBuilder.group({
      invoiceNumber: ['', Validators.compose([Validators.required])],
      mobileId: ['', Validators.compose([Validators.required])],
      cardId: ['', Validators.compose([])],
      amount: ['', Validators.compose([Validators.required,Validators.pattern('^[0-9]*$')])],
    })
  }

  get inquiryInvoiceNumber() {return this.inquiryForm.get('invoiceNumber')}
  get inquiryMobileNumber() {return this.inquiryForm.get('mobileId')} 
  get inquiryCardNumber() {return this.inquiryForm.get('cardNumber')}

  get payInvoiceNumber() {return this.payForm.get('invoiceNumber')} 
  get payMobileNumber() {return this.payForm.get('mobileId')}
  get payAmount() {return this.payForm.get('amount')}
  get payCardId(){return this.payForm.get('cardId')}
 
  async getMobileNumbers(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getMobileAccounts({userId: userId});
    console.log(result);

    if(result && result.success === true){
      this.mobileNumbers = result.result;
      console.log('mobileNumbers', this.mobileNumbers);
      this.getCards();
    }
  }

  async getCards(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getCards({userId: userId});
    console.log(result);

    if(result && result.success === true){
      this.cards = result.result;
      console.log('cards', this.cards);
    }
  }

  async onSubmit(){
    if(this.isInquiry){
      this.inquirySubmitted = true;
        if(this.inquiryForm.invalid) return;
        
        var userId = await this.sqlite.getActiveUserId();
        let obj = this.inquiryForm.value;
        obj['userId'] = userId;

        let result = await this.network.InquiryE15Services(obj);
        if(result && result.success === true){
          this.utility.presentToast("Inquiry received");
          this.popToRoot();
        }

    } else {
      this.paySubmitted = true;
        if(this.payForm.invalid) return;
        if(this.cardSelected && !this.payCardId.value) return;
        
        let obj = this.payForm.value;
        var userId = await this.sqlite.getActiveUserId();
        obj['userId'] = userId;
        obj['PaymentMethod'] = this.cardSelected ? 'Card' : 'Mobile Wallet';

        let result = await this.network.payE15Service(obj);
        if(result && result.success === true){
          this.utility.presentToast("Success");
          this.events.publish('balanceRefresh');
          this.popToRoot();
        }

    }
  }

  popToRoot(){
    this.nav.navigateTo('pages/home')
  }

}

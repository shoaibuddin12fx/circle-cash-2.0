import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CardTransferDetailsPageRoutingModule } from './card-transfer-details-routing.module';

import { CardTransferDetailsPage } from './card-transfer-details.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardTransferDetailsPageRoutingModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [CardTransferDetailsPage]
})
export class CardTransferDetailsPageModule {}

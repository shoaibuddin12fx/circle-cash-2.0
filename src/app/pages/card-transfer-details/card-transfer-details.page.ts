import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-card-transfer-details',
  templateUrl: './card-transfer-details.page.html',
  styleUrls: ['./card-transfer-details.page.scss'],
})
export class CardTransferDetailsPage extends BasePage implements OnInit {

  data;
  date;
  constructor(injector:Injector) 
  {
    super(injector)
  }

  ngOnInit() {
    this.data = this.nav.getQueryParams();
    console.log(this.data);
    this.date = new Date();
  }

  async onConfirm() {
   let obj = this.data;
   let result = await this.network.addCardTransfer(obj);
   if(result && result.success === true){
    this.utility.presentToast("Success")
    this.nav.navigateTo("pages/home");
   }

    this.utility.hideLoader();
  }

}

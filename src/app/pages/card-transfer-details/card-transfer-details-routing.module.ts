import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';

import { CardTransferDetailsPage } from './card-transfer-details.page';

const routes: Routes = [
  {
    path: '',
    component: CardTransferDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  ],
  exports: [RouterModule],

})
export class CardTransferDetailsPageRoutingModule {}

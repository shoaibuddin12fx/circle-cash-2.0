import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PrepaidElectricityBillPage } from './prepaid-electricity-bill.page';

describe('PrepaidElectricityBillPage', () => {
  let component: PrepaidElectricityBillPage;
  let fixture: ComponentFixture<PrepaidElectricityBillPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PrepaidElectricityBillPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PrepaidElectricityBillPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

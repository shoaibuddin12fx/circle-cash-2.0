import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { NavigationExtras } from '@angular/router';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-prepaid-electricity-bill',
  templateUrl: './prepaid-electricity-bill.page.html',
  styleUrls: ['./prepaid-electricity-bill.page.scss'],
})
export class PrepaidElectricityBillPage extends BasePage implements OnInit {
  cardSelected = true;
  meterNumbers = [];
  cards = [];
  aForm: FormGroup;
  submitted = false;

  constructor(injector: Injector) {
    super(injector);
    this.setupForm();
    this.getMeterNumbers();
    this.getCards();
  }

  setupForm(){
    this.aForm = this.formBuilder.group({
      meterId: ['', Validators.compose([Validators.required ]) ],
      amount: ['', Validators.compose([Validators.required,Validators.pattern('^[0-9]*$') ]) ],
      cardId: ['', Validators.compose([])],
    })
  }

  get meterId() {return this.aForm.get('meterId')}
  get amount() {return this.aForm.get('amount')}
  get cardId() {return this.aForm.get('cardId')}

  ngOnInit() {
  }

  navigate(path){
    this.nav.navigateTo(path)
  }

  async getCards(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getCards({userId: userId});
    console.log(result);

    if(result && result.success === true){
      this.cards = result.result;
      console.log('cards', this.cards);
    }
  }

  async getMeterNumbers(){
    var userId = await this.sqlite.getActiveUserId();

    let result = await this.network.getMeterAccounts({userId: userId});
    console.log(result);
    if(result && result.success === true)
      this.meterNumbers = result.result;
      console.log('meterNumbers', this.meterNumbers);
  }

  OnCardSelected(selected){
    this.cardSelected = selected;
    
  }

  async onSubmit() {
    this.submitted = true;
    if(this.aForm.invalid || (this.cardSelected && !this.cardId.value)) return;
    var obj = this.aForm.value;
    obj.PaymentMethod = this.cardSelected ? 'Card' : 'Mobile Wallet';
    
    if(this.cardSelected)
      obj["cardNumber"] = this.cards.filter(x=> x._id === this.cardId.value)[0].cardNumber;
    
    obj["meterNumber"] = this.meterNumbers.filter(x=> x._id === this.meterId.value)[0].meterNumber;
    console.log(obj)
    this.nav.push('pages/prepaid-electricity-bill-confirm', obj)
    // var result = await this.network.rechargeElectricityMeter(obj);
    // console.log('onFormSubmit', result);

    // if(result.success == true){
    //   this.utility.presentToast("An OTP has been sent to your number");
    //   //TODO Configure some Wait...
    //   this.navigate('pages/verify-account')
    // }
    
  }

}
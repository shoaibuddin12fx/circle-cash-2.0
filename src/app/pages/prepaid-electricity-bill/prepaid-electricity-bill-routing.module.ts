import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrepaidElectricityBillPage } from './prepaid-electricity-bill.page';

const routes: Routes = [
  {
    path: '',
    component: PrepaidElectricityBillPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrepaidElectricityBillPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLocalBankAccountConfirmationPageRoutingModule } from './add-local-bank-account-confirmation-routing.module';

import { AddLocalBankAccountConfirmationPage } from './add-local-bank-account-confirmation.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddLocalBankAccountConfirmationPageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [AddLocalBankAccountConfirmationPage]
})
export class AddLocalBankAccountConfirmationPageModule {}

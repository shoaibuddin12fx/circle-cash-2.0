import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLocalBankAccountConfirmationPage } from './add-local-bank-account-confirmation.page';

const routes: Routes = [
  {
    path: '',
    component: AddLocalBankAccountConfirmationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddLocalBankAccountConfirmationPageRoutingModule {}

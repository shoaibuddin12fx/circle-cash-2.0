import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddLocalBankAccountConfirmationPage } from './add-local-bank-account-confirmation.page';

describe('AddLocalBankAccountConfirmationPage', () => {
  let component: AddLocalBankAccountConfirmationPage;
  let fixture: ComponentFixture<AddLocalBankAccountConfirmationPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLocalBankAccountConfirmationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddLocalBankAccountConfirmationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

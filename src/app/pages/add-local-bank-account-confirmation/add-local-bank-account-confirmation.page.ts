import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-add-local-bank-account-confirmation',
  templateUrl: './add-local-bank-account-confirmation.page.html',
  styleUrls: ['./add-local-bank-account-confirmation.page.scss'],
})
export class AddLocalBankAccountConfirmationPage extends BasePage implements OnInit {
  data;

  constructor(injector:Injector) 
  {
    super(injector);
  }

  ngOnInit() {
    const res = this.nav.getQueryParams();
    console.log(res);
    this.data = Object.assign({}, res);
    console.log("data",this.data);
  }

  async onConfirm(){
    let result = await this.network.addBankAccount(this.data);
    if(result && result.success === true) {
      this.utility.presentToast("Success");
      this.nav.navigateTo('pages/home')
    }

  }

}

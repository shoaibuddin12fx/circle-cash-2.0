import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-prepaid-electricity-bill-confirm',
  templateUrl: './prepaid-electricity-bill-confirm.page.html',
  styleUrls: ['./prepaid-electricity-bill-confirm.page.scss'],
})
export class PrepaidElectricityBillConfirmPage extends BasePage implements OnInit {
  billData;
  constructor(injector: Injector) {
    super(injector);
   }

  ngOnInit() {
    const res = this.nav.getQueryParams();
    this.billData = Object.assign({}, res);
  }

  navigate(path){
    this.nav.navigateTo(path)
  }

  async onConfirmPay() {
    var userId = await this.sqlite.getActiveUserId();
    this.billData['userId'] = userId;
    var result = await this.network.rechargeElectricityMeter(this.billData);
    console.log('onFormSubmit', result);

    if(result.success == true){
      this.events.publish('balanceRefresh');
      this.utility.presentToast("Bill Payment made successfully");
      this.nav.navigateTo("pages/home")
    }
  }

}

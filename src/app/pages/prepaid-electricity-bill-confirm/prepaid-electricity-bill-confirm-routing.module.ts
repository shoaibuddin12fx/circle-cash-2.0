import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrepaidElectricityBillConfirmPage } from './prepaid-electricity-bill-confirm.page';

const routes: Routes = [
  {
    path: '',
    component: PrepaidElectricityBillConfirmPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrepaidElectricityBillConfirmPageRoutingModule {}

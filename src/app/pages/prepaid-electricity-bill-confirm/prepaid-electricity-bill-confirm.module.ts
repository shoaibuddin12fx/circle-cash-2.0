import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrepaidElectricityBillConfirmPageRoutingModule } from './prepaid-electricity-bill-confirm-routing.module';

import { PrepaidElectricityBillConfirmPage } from './prepaid-electricity-bill-confirm.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrepaidElectricityBillConfirmPageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [PrepaidElectricityBillConfirmPage]
})
export class PrepaidElectricityBillConfirmPageModule {}

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PrepaidElectricityBillConfirmPage } from './prepaid-electricity-bill-confirm.page';

describe('PrepaidElectricityBillConfirmPage', () => {
  let component: PrepaidElectricityBillConfirmPage;
  let fixture: ComponentFixture<PrepaidElectricityBillConfirmPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PrepaidElectricityBillConfirmPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PrepaidElectricityBillConfirmPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

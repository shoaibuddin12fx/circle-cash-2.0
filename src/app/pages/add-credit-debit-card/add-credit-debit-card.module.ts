import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddCreditDebitCardPageRoutingModule } from './add-credit-debit-card-routing.module';

import { AddCreditDebitCardPage } from './add-credit-debit-card.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddCreditDebitCardPageRoutingModule,
    ReactiveFormsModule,
    HeaderBarModule,
    FooterModule,
    TranslateModule
  ],
  declarations: [AddCreditDebitCardPage]
})
export class AddCreditDebitCardPageModule {}

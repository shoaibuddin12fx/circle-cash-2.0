import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-add-credit-debit-card',
  templateUrl: './add-credit-debit-card.page.html',
  styleUrls: ['./add-credit-debit-card.page.scss'],
})

export class AddCreditDebitCardPage extends BasePage implements OnInit {
  aForm: FormGroup;
  submitted = false;
  years = [];
  data;
  months = ["Jan","Feb", "Mar","Apr","May","Jun", "Jul","Aug","Sep","Oct","Nov","Dec"]
  
  constructor(injector:Injector) {
    super(injector)
   }

  ngOnInit() {
    this.setupForm();
    this.init();
    let params = this.nav.getQueryParams();
    this.data = Object.assign({}, params)
    this.cardNumber.setValue(params.cardNumber)
    this.name.setValue(params.nameOnCard)
    this.month.setValue(params.expiryMonth)
    this.year.setValue(params.expiryYear)
  }

  setupForm() {
    this.aForm = this.formBuilder.group({
      cardNumber: ['', Validators.compose([Validators.required, Validators.minLength(18), Validators.maxLength(19)])],
      nameOnCard: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      expiryYear: ['', Validators.compose([Validators.required])],
      expiryMonth: ['', Validators.compose([Validators.required])],
      cvv : ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(3)])]
    });
  }

  get cardNumber () {return this.aForm.get('cardNumber')}
  get name () {return this.aForm.get('nameOnCard')}
  get year() {return this.aForm.get('expiryYear')}
  get month() {return this.aForm.get('expiryMonth')}
  get cvv() {return this.aForm.get('cvv')}

  init() {
    let year = new Date().getFullYear();
    for(var i = 0; i < 20; i ++) {
      this.years.push(year+i); 
    }
  }

  async onSubmit() {
    this.submitted = true;
    if(this.aForm.invalid) return;
    var obj = this.aForm.value;
    var userId = await this.sqlite.getActiveUserId();
    obj['userId'] = userId;
    var result;

    if(this.data && this.data._id){
      obj["_id"] = this.data._id;
      result = await this.network.updateCard(obj);
    }

    else result = await this.network.addCard(obj);

    if(result.success === true){
      this.utility.presentToast("Success")
      this.popToRoot();
    }
  }

  onCardNumberChange($event){

    if ($event.inputType !== 'deleteContentBackward') {
      const utel = this.utility.onkeyupFormatCardNumberRuntime($event.target.value, false);
      console.log(utel);
      $event.target.value = utel;
      this.aForm.controls['cardNumber'].patchValue(utel);
      // $event.target.value = utel;
    }

  }

}

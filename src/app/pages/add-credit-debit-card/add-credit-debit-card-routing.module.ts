import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddCreditDebitCardPage } from './add-credit-debit-card.page';

const routes: Routes = [
  {
    path: '',
    component: AddCreditDebitCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddCreditDebitCardPageRoutingModule {}

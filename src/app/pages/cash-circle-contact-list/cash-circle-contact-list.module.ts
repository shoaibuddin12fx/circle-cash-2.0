import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CashCircleContactListPageRoutingModule } from './cash-circle-contact-list-routing.module';

import { CashCircleContactListPage } from './cash-circle-contact-list.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CashCircleContactListPageRoutingModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [CashCircleContactListPage]
})
export class CashCircleContactListPageModule {}

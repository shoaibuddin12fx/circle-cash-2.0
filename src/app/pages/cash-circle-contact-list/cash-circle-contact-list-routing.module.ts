import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CashCircleContactListPage } from './cash-circle-contact-list.page';

const routes: Routes = [
  {
    path: '',
    component: CashCircleContactListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CashCircleContactListPageRoutingModule {}

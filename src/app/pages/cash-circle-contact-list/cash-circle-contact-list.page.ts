import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-cash-circle-contact-list',
  templateUrl: './cash-circle-contact-list.page.html',
  styleUrls: ['./cash-circle-contact-list.page.scss'],
})
export class CashCircleContactListPage extends BasePage implements OnInit {
  dataList = []

  constructor(injector:Injector) {super(injector)}

  ngOnInit() {
    this.getContacts();
  }

  async getContacts(){
    var userId = await this.sqlite.getActiveUserId();
    let obj = { userId: userId}
    let result = await this.network.getCircleCashContactList(obj);
    if(result && result.success === true){
      console.log(result.result);
      this.dataList = result.result;
    }
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditPayeeDetailPageRoutingModule } from './edit-payee-detail-routing.module';

import { EditPayeeDetailPage } from './edit-payee-detail.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditPayeeDetailPageRoutingModule,
    HeaderBarModule,
    FooterModule,
    TranslateModule,
    ReactiveFormsModule
  ],
  declarations: [EditPayeeDetailPage]
})
export class EditPayeeDetailPageModule {}

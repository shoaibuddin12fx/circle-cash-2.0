import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditPayeeDetailPage } from './edit-payee-detail.page';

const routes: Routes = [
  {
    path: '',
    component: EditPayeeDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditPayeeDetailPageRoutingModule {}

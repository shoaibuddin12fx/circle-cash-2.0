import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';


@Component({
  selector: 'app-edit-payee-detail',
  templateUrl: './edit-payee-detail.page.html',
  styleUrls: ['./edit-payee-detail.page.scss'],
})
export class EditPayeeDetailPage extends BasePage implements OnInit {

  
  aForm: FormGroup;
  submitted = false;
  constructor(injector:Injector)
  {
    super(injector);
  }


  ngOnInit() {
    this.setupForm();
  }


  setupForm(){
    this.aForm = this.formBuilder.group({
      cashCircle: ['', Validators.compose([Validators.required,Validators.minLength(4),Validators.maxLength(6)])],
      beneficiaryName: ['', Validators.compose([Validators.required])],
      mobileNum: ['', Validators.compose([Validators.required])],
    });
  }

  get cashCircle(){return this.aForm.get('cashCircle')}
  get beneficiaryName(){return this.aForm.get('beneficiaryName')}
  get mobileNum(){return this.aForm.get('mobileNum')}

  onSubmit(){
    this.submitted = true;
  }
}

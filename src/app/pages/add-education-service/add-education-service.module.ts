import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddEducationServicePageRoutingModule } from './add-education-service-routing.module';

import { AddEducationServicePage } from './add-education-service.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddEducationServicePageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [AddEducationServicePage]
})
export class AddEducationServicePageModule {}

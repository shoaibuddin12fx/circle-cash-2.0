import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-add-education-service',
  templateUrl: './add-education-service.page.html',
  styleUrls: ['./add-education-service.page.scss'],
})
export class AddEducationServicePage extends BasePage implements OnInit {
  sudaneseForm: FormGroup;
  arabForm: FormGroup;
  isArab: boolean = false;
  submitted;

  constructor(injector:Injector) {
    super(injector);
    this.setupSudaneseForm();
    this.setupArabForm();

   }

  ngOnInit() {
  }

  setupArabForm() {
    this.arabForm = this.formBuilder.group({
      studentName: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      selectedForm: ['', Validators.compose([Validators.required])],
      courseType: ['', Validators.compose([Validators.required])],
      amount: ['', Validators.compose([Validators.required,Validators.pattern('^[0-9]*$')]) ]
    })
  }

  get arabFormStudentName() {return this.arabForm.get('studentName')}
  get arabFormAdmissionType() {return this.arabForm.get('selectedForm')}
  get arabFormCourseType() {return this.arabForm.get('courseType')}
  get arabFormAmount() {return this.arabForm.get('amount')}

  setupSudaneseForm(){
    this.sudaneseForm = this.formBuilder.group({
      studentName: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      selectedForm: ['', Validators.compose([Validators.required])],
      courseType: ['', Validators.compose([Validators.required])],
      seatNumber: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      amount: ['', Validators.compose([Validators.required,Validators.pattern('^[0-9]*$')]) ]
    })
  }

  get sudaneseFormStudentName() {return this.sudaneseForm.get('studentName')}
  get sudaneseFormAdmissionType() {return this.sudaneseForm.get('selectedForm')} 
  get sudaneseFormCourseType() {return this.sudaneseForm.get('courseType')}
  get sudaneseFormSeatNumber() {return this.sudaneseForm.get('seatNumber')}
  get sudaneseFormAmount() {return this.sudaneseForm.get('amount')}

  async onSubmit(){
    if(this.isArab){
      
    if(this.arabForm.invalid) return;
    
    var obj = this.arabForm.value;
    var userId = await this.sqlite.getActiveUserId();
    console.log(userId);
    obj['userId'] = userId;
    obj['billType'] = "SUDANESE";
    obj['rechargeType'] = "MOBILE_WALLET";
    let result = await this.network.addEducationPayment2(obj);
    if(result && result.success === true)
      this.utility.presentToast("Success");  

    } else {
      if(this.sudaneseForm.invalid) return;

    var obj = this.sudaneseForm.value;
    var userId = await this.sqlite.getActiveUserId();
    obj['userId'] = userId;
    obj["billType"] = "ARAB";
    obj["rechargeType"] = "MOBILE_WALLET";

    let result = await this.network.addEducationPayment2(obj);
    if(result && result.success === true)
      this.utility.presentToast("Success");  

    }
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddEducationServicePage } from './add-education-service.page';

const routes: Routes = [
  {
    path: '',
    component: AddEducationServicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddEducationServicePageRoutingModule {}

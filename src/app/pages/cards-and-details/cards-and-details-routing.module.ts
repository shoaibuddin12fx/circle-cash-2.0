import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CardsAndDetailsPage } from './cards-and-details.page';

const routes: Routes = [
  {
    path: '',
    component: CardsAndDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardsAndDetailsPageRoutingModule {}

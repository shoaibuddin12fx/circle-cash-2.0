import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-cards-and-details',
  templateUrl: './cards-and-details.page.html',
  styleUrls: ['./cards-and-details.page.scss'],
})
export class CardsAndDetailsPage extends BasePage implements OnInit {
  cards = [];
  @ViewChild("slides") slides:IonSlides;
  slideOpts = {
    slidesPerView: 3,
    coverflowEffect: {
      rotate: 50,
      stretch: 0,
      depth: 100,
      modifier: 1,
      slideShadows: true,
    }
  }

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
     this.getCards();
  }

  async navigate(path){
    if(!this.cards || !this.cards.length) return;

    let slideNum = await this.slides.getActiveIndex();
    console.log(slideNum);

    if(this.cards && this.cards.length){
      let card = this.cards[slideNum];
      this.nav.push(path,card)
    } else this.nav.push(path);
    
  }

  async getCards(){
    var userId = await this.sqlite.getActiveUserId();
    let cardsResult = await this.network.getCards({userId: userId});
    console.log(cardsResult);
    if(cardsResult && cardsResult.success == true){
      this.cards = cardsResult.result;
    }
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CardsAndDetailsPageRoutingModule } from './cards-and-details-routing.module';

import { CardsAndDetailsPage } from './cards-and-details.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardsAndDetailsPageRoutingModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [CardsAndDetailsPage]
})
export class CardsAndDetailsPageModule {}

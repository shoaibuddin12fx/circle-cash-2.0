import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GenerateVoucherDetailsPageRoutingModule } from './generate-voucher-details-routing.module';

import { GenerateVoucherDetailsPage } from './generate-voucher-details.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GenerateVoucherDetailsPageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [GenerateVoucherDetailsPage]
})
export class GenerateVoucherDetailsPageModule {}

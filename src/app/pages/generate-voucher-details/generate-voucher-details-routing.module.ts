import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GenerateVoucherDetailsPage } from './generate-voucher-details.page';

const routes: Routes = [
  {
    path: '',
    component: GenerateVoucherDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GenerateVoucherDetailsPageRoutingModule {}

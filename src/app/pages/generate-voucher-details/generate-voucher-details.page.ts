import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-generate-voucher-details',
  templateUrl: './generate-voucher-details.page.html',
  styleUrls: ['./generate-voucher-details.page.scss'],
})
export class GenerateVoucherDetailsPage extends BasePage implements OnInit {

  data; 
  date;
  constructor(injector:Injector) {super(injector) }

  ngOnInit() {
    this.data = this.nav.getQueryParams();
    this.date = new Date();
  }

  async onConfirm(){
   let result = await this.network.generateVoucher(this.data);
   if(result && result.success === true){
     this.utility.presentToast("Success");
     this.nav.navigateTo('pages/home');
   }
  }

}

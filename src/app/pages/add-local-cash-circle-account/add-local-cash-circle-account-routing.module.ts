import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLocalCashCircleAccountPage } from './add-local-cash-circle-account.page';

const routes: Routes = [
  {
    path: '',
    component: AddLocalCashCircleAccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddLocalCashCircleAccountPageRoutingModule {}

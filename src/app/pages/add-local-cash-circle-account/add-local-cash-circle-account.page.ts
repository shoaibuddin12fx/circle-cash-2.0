import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-add-local-cash-circle-account',
  templateUrl: './add-local-cash-circle-account.page.html',
  styleUrls: ['./add-local-cash-circle-account.page.scss'],
})
export class AddLocalCashCircleAccountPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitted = false

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  this.setupform();
  }

  setupform(){
    this.aForm = this.formBuilder.group({
        circleCashId:['', Validators.compose([Validators.required, Validators.minLength(4)])],
        BenefeciaryName:['', Validators.compose([Validators.required, Validators.minLength(3)])],
        mobileNumber:['',Validators.compose([Validators.required, Validators.minLength(7), Validators.maxLength(12), Validators.pattern('^[0-9]*$'), ])]
    });
  }

  get cashId() {return this.aForm.get('circleCashId')};
  get BenefeciaryName() {return this.aForm.get('BenefeciaryName')};
  get mobileNumber(){return this.aForm.get('mobileNumber')};

  navigate(path){
    this.nav.navigateTo(path)
  }

  async onSubmit(){

    this.submitted = true;
    if(this.aForm.invalid) return;
    let obj = this.aForm.value;
    let result = await this.network.getCircleCashUser(obj);
    if(result && result.success === true){
      console.log(result)
      obj["fullName"] = result.result.fullName;
      this.nav.push('pages/local-cash-circle-account', obj)
    }
      
   
    

  }

}

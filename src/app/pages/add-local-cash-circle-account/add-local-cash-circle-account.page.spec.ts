import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddLocalCashCircleAccountPage } from './add-local-cash-circle-account.page';

describe('AddLocalCashCircleAccountPage', () => {
  let component: AddLocalCashCircleAccountPage;
  let fixture: ComponentFixture<AddLocalCashCircleAccountPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AddLocalCashCircleAccountPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddLocalCashCircleAccountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

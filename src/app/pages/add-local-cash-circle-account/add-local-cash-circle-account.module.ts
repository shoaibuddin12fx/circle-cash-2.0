import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLocalCashCircleAccountPageRoutingModule } from './add-local-cash-circle-account-routing.module';

import { AddLocalCashCircleAccountPage } from './add-local-cash-circle-account.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddLocalCashCircleAccountPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [AddLocalCashCircleAccountPage]
})
export class AddLocalCashCircleAccountPageModule {}

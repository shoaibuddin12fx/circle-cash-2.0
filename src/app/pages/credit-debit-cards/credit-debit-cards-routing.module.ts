import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreditDebitCardsPage } from './credit-debit-cards.page';

const routes: Routes = [
  {
    path: '',
    component: CreditDebitCardsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreditDebitCardsPageRoutingModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-credit-debit-cards',
  templateUrl: './credit-debit-cards.page.html',
  styleUrls: ['./credit-debit-cards.page.scss'],
})
export class CreditDebitCardsPage extends BasePage implements OnInit {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

  navigate(path){
    this.nav.navigateTo(path)
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreditDebitCardsPageRoutingModule } from './credit-debit-cards-routing.module';

import { CreditDebitCardsPage } from './credit-debit-cards.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreditDebitCardsPageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [CreditDebitCardsPage]
})
export class CreditDebitCardsPageModule {}

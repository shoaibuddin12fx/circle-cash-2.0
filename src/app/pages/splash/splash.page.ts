import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-splash',
  templateUrl: './splash.page.html',
  styleUrls: ['./splash.page.scss'],
})
export class SplashPage extends BasePage implements OnInit {

  loading = true;
  constructor(injector: Injector, private menu: MenuController) {
    super(injector);
    this.initialize();
  }

  async initialize(){

    this.loading = true;
    await this.platform.ready();
    // await this.sqlite.initialize();
    this.loading = false;
    




  }

  ngOnInit() {
    this.menu.enable(false,"drawer");
  }

  async moveToLogin(){

    const userId = await this.sqlite.getActiveUserId();
    if(userId){
      this.nav.push('pages/home')
    }else{
      this.nav.push('pages/login')
    }
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccountTransferPageRoutingModule } from './account-transfer-routing.module';

import { AccountTransferPage } from './account-transfer.page';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccountTransferPageRoutingModule,
    HeaderBarModule,
    FooterModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [AccountTransferPage]
})
export class AccountTransferPageModule {}

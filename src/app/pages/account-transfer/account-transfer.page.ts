import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-account-transfer',
  templateUrl: './account-transfer.page.html',
  styleUrls: ['./account-transfer.page.scss'],
})
export class AccountTransferPage extends BasePage implements OnInit {
  aForm:FormGroup;
  cards = [];
  submitted = false;

  constructor(injector: Injector) {
    super(injector);
   }

  ngOnInit() {
    this.setupForm();
    this.getCards();
  }

  async getCards(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getCards({userId: userId});
    console.log(result);

    if(result && result.success === true){
      this.cards = result.result;
      console.log('cards', this.cards);
    }
  }

  setupForm(){
    this.aForm = this.formBuilder.group({
      amount: ['', Validators.compose([Validators.required])],
      cardId: ['', Validators.compose([Validators.required])],
      iPin: ['', Validators.compose([Validators.required])],
      to: ['', Validators.compose([Validators.required])],
    });
  }

  get amount () {return this.aForm.get('amount')}
  get cardId () {return this.aForm.get('cardId')}
  get iPin () {return this.aForm.get('iPin')}
  get to () {return this.aForm.get('to')}

  async onSubmit(){
    this.submitted = true;
    if(this.aForm.invalid) return;
    var userId = await this.sqlite.getActiveUserId();
    let obj = this.aForm.value;
    obj["userId"] = userId;
    obj["cardNumber"] = this.cards.filter(x => x._id == this.cardId.value)[0].cardNumber;
    let result = await this.network.accountTransfer(obj)
    if(result && result.success === true){
      this.utility.presentToast("Success")
      this.popToRoot();
    }

  }

}

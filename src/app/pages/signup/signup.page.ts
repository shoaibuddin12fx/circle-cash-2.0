import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage extends BasePage implements OnInit {
  aForm: FormGroup;
  submitted = false;
  constructor(injector: Injector) {
    super(injector);
    this.setupForm();
  }

  ngOnInit() {

  }

  setupForm(){
    this.aForm = this.formBuilder.group({
      fullName: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
      mobileNumber: ['', Validators.compose([Validators.required,Validators.pattern(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/), Validators.minLength(9) ]) ],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)]) ]
    })
  }

  get fullName() { return this.aForm.get('fullName') }
  get mobileNumber() { return this.aForm.get('mobileNumber') }
  get password() { return this.aForm.get('password') }

  navigate(path){
    this.nav.navigateTo(path)
  }

  async onFormSubmit() {
    this.submitted = true;
    if(this.aForm.invalid) return;
    var obj = this.aForm.value;
    var result = await this.network.register(obj);
    console.log('onFormSubmit', result); 

    if(result && result.success == true){
      await this.users.LoginUser(result);
      this.events.publish('login_event');
      //this.utility.presentToast("An OTP has been sent to your number");
      //TODO Configure some Wait...
      //this.navigate('pages/verify-account')
    }
  }

  onTelephoneChange($event){

    if ($event.inputType !== 'deleteContentBackward') {
      const utel = this.utility.onkeyupFormatPhoneNumberRuntime($event.target.value, false);
      console.log(utel);
      $event.target.value = utel;
      this.aForm.controls['mobileNumber'].patchValue(utel);
      // $event.target.value = utel;
    }

  }
}
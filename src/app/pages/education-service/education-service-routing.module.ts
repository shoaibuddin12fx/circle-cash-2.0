import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EducationServicePage } from './education-service.page';

const routes: Routes = [
  {
    path: '',
    component: EducationServicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EducationServicePageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EducationServicePageRoutingModule } from './education-service-routing.module';

import { EducationServicePage } from './education-service.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EducationServicePageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [EducationServicePage]
})
export class EducationServicePageModule {}

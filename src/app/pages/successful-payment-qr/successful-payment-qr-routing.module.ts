import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SuccessfulPaymentQrPage } from './successful-payment-qr.page';

const routes: Routes = [
  {
    path: '',
    component: SuccessfulPaymentQrPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SuccessfulPaymentQrPageRoutingModule {}

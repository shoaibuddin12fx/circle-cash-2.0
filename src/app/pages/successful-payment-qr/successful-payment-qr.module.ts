import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SuccessfulPaymentQrPageRoutingModule } from './successful-payment-qr-routing.module';

import { SuccessfulPaymentQrPage } from './successful-payment-qr.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SuccessfulPaymentQrPageRoutingModule,
    FooterModule
  ],
  declarations: [SuccessfulPaymentQrPage, HeaderComponent]
})
export class SuccessfulPaymentQrPageModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage extends BasePage implements OnInit {
  aForm: FormGroup;
  submitted = false;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.setupForm()
  }

  setupForm() {
    this.aForm = this.formBuilder.group({
      mobileNumber: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
    })
  }
  get mobileNumber() {return this.aForm.get('mobileNumber')}

  navigate(){
    this.nav.navigateTo('pages/verify-account')
  }

  async onSubmit(){
    this.submitted = true;
    if(!this.aForm.invalid){
      let obj = this.aForm.value; 
      let userId = await this.sqlite.getActiveUserId();
      obj['userId'] = userId;
      let result = await this.network.forgotPasswordCircleCash(obj);
      
      if(result && result.success === true){
        let otp = result.result.otp;
        console.log(otp, otp);
        let obj = {otp: otp,mobileNumber:this.mobileNumber.value}
       this.nav.push('pages/verify-account',obj)
      }
        
    } else 
    { 
      console.log('invalid');
      console.log(this.mobileNumber.value);
    }
    
  }

}

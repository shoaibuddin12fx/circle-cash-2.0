import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GeneratePinPageRoutingModule } from './generate-pin-routing.module';

import { GeneratePinPage } from './generate-pin.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GeneratePinPageRoutingModule,
    HeaderBarModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  declarations: [GeneratePinPage]
})
export class GeneratePinPageModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-generate-pin',
  templateUrl: './generate-pin.page.html',
  styleUrls: ['./generate-pin.page.scss'],
})
export class GeneratePinPage extends BasePage implements OnInit {
  aForm: FormGroup;
  submitted = false;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.setupForm();
  }

  setupForm() {
    this.aForm = this.formBuilder.group({
      mobileNumber: ['', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])],
    })
  }

  get mobileNumber() {return this.aForm.get('mobileNumber')}

  async onSubmit(){
    this.submitted = true;
    if(!this.aForm.invalid){
      let obj = this.aForm.value;
      let userId = await this.sqlite.getActiveUserId();
      obj['userId'] = userId;
      let result = await this.network.verifyMobileNumber(obj);
      
      if(result && result.success === true){
        let otp = result.result;
        let obj = this.aForm.value;
        obj["otp"] = otp;
        this.nav.push('pages/otp-verify',obj)
      }
        
    }
    
  }

}

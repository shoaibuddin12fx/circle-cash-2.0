import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-telecom-service-providers',
  templateUrl: './telecom-service-providers.page.html',
  styleUrls: ['./telecom-service-providers.page.scss'],
})
export class TelecomServiceProvidersPage extends BasePage implements OnInit {
  submitted = false;
  aForm: FormGroup;
  constructor(injector:Injector) 
  {
    super(injector)
  }
  
  ngOnInit() {
    this.setupForm();
  }

  setupForm(){
    this.aForm = this.formBuilder.group({
      mobileNumber: ['', Validators.compose([Validators.required,Validators.pattern(/^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$/), Validators.minLength(9) ]) ],
      connectionType: ['', Validators.compose([Validators.required]) ]
    })
  }
  get mobileNumber () {return this.aForm.get('mobileNumber')}
  get connectionType () {return this.aForm.get('connectionType')}
  
  async onSubmit(){
    this.submitted = true;
    if(this.aForm.invalid) return;
    var obj = this.aForm.value;
    var userId = await this.sqlite.getActiveUserId();
    obj['userId'] = userId;
    let result = await this.network.addMobileAccount(obj);
    if(result && result.success === true){
      this.utility.presentToast("Success");
    }
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TelecomServiceProvidersPage } from './telecom-service-providers.page';

const routes: Routes = [
  {
    path: '',
    component: TelecomServiceProvidersPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TelecomServiceProvidersPageRoutingModule {}

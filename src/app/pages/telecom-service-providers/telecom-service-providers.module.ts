import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TelecomServiceProvidersPageRoutingModule } from './telecom-service-providers-routing.module';

import { TelecomServiceProvidersPage } from './telecom-service-providers.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TelecomServiceProvidersPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [TelecomServiceProvidersPage]
})
export class TelecomServiceProvidersPageModule {}

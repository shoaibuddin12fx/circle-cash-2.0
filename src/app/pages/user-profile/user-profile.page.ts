import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { BasePage } from '../base-page/base-page';
import { TranslateService } from '@ngx-translate/core';
import { ActionSheetController } from '@ionic/angular';



@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage extends BasePage implements OnInit {

  uForm: FormGroup;
  submitted = false;
  profilePic;
  gender = 1; // 1 = Male & Female = 2

  constructor(injector:Injector, private translateService:TranslateService, private actionSheetController:ActionSheetController)
  {
    super(injector);
  }

  ngOnInit() {
    this.setupForm();
    this.getProfile();
  }

  setupForm(){
    this.uForm = this.formBuilder.group({
      fullName: ['', Validators.compose([Validators.required,Validators.minLength(3)])],
      dob: ['1997-09-02', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
    });
  }

    get fullName(){return this.uForm.get('fullName')}
    get address(){return this.uForm.get('address')}
    get dob() {return this.uForm.get('dob')}
    
  navigate(path){
    this.nav.navigateTo(path)
  }

  logout(){
    this.users.logout();
  }

  async getProfile(){
    var userId = await this.sqlite.getActiveUserId();
    let result = await this.network.getProfile('?userId='+userId);
    if(result && result.success === true){
      let user = result.result;
      this.fullName.setValue(user.fullName);
      this.profilePic = user.profilePic;
      this.address.setValue(user.address)
      this.dob.setValue(user.dob)
      if(user.gender) this.gender = user.gender;
    }

    // let data = await this.getProfileData();
    console.log(result);
  }


  async onSubmit(){
    this.submitted = true;
    if(this.uForm.invalid) return;
    var userId = await this.sqlite.getActiveUserId();
    let obj = this.uForm.value;
    obj["userId"] = userId;
    obj["gender"] = this.gender;
    obj["profilePic"] = this.profilePic;
    
    if(this.hasWhiteSpace(this.fullName.value)){
      let fullName = this.fullName.value.split(" ")
      obj["firstName"] = fullName[0];
      obj["lastName"] = fullName[1];
    }


    let result = await this.network.editsettingInformation(obj);
    if(result && result.success === true){
      this.utility.presentToast("Success");
      let user = result.result;
      this.users.setLocalUser(user);
      this.events.publish("login_event")
      this.events.publish('balanceRefresh');
      this.nav.pop();
    }
    // this.events.publish("user-profile:update event");
    console.log(result);
  }


  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      // header: "Select Image source",
      cssClass: 'select-image-css',
      buttons: [{
        // text: 'Choose photo from Gallery',
        text: this.translateService.instant("Choose_photo_from_Gallery"),
        icon: 'images',
        cssClass: 'images-icon',
        handler: () => {
          this.setProfileImage('Gallery')
        }
      },
      {
        // text: 'Take photo',
        text: this.translateService.instant("Take_photo"),
        icon: 'camera',
        cssClass: 'camera-icon',
        handler: () => {
          this.setProfileImage('Camera')
        }
      },
      {
        // text: 'Cancel',
        text: this.translateService.instant("Cancel"),
        role: 'cancel',
        icon: 'close',
        cssClass: 'close-icon'
      }
      ]
    });
    await actionSheet.present();
  }

  async setProfileImage(type){

    const image = await this.utility.snapImage(type);
    if(image){
      this.profilePic = image;    
    }
  }
  
  // getProfileData():Promise<any>{
  //   return new Promise((resolve,reject)=>{
  //     this.network.getProfile().then(result=>{
  //       var result = result;
  //       resolve(result);
  //     }).catch(err => {
  //       reject();
  //     })
  //   })
  // }
  hasWhiteSpace(s) {
    return s.indexOf(' ') >= 0;
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UserProfilePageRoutingModule } from './user-profile-routing.module';

import { UserProfilePage } from './user-profile.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { TranslateModule } from '@ngx-translate/core';
import { Camera } from '@ionic-native/camera/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UserProfilePageRoutingModule,    
    HeaderBarModule,
    FooterModule,
    TranslateModule,
    ReactiveFormsModule,
  ],
  declarations: [UserProfilePage],
  exports: [UserProfilePage]
})
export class UserProfilePageModule {}

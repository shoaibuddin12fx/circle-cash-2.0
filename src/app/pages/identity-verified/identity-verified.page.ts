import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-identity-verified',
  templateUrl: './identity-verified.page.html',
  styleUrls: ['./identity-verified.page.scss'],
})
export class IdentityVerifiedPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitted = false;
  mobileNumber;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.setupForm()
    let params = this.nav.getQueryParams();
    console.log(params);
    this.mobileNumber = params.mobileNumber;
  }

  setupForm() {
    this.aForm = this.formBuilder.group({
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    })
  }

  get password() {return this.aForm.get('password')}

  navigate(path){
    this.nav.navigateTo(path)
  }

  async onSubmit(){
    this.submitted = true;
    if(!this.aForm.invalid){
      let obj = this.aForm.value; 
      obj['mobileNumber'] = this.mobileNumber;
      let result = await this.network.changePasswordVerified(obj);
      
      if(result && result.success === true){
        this.navigate('pages/new-password-generated')
      }
        
    }
    
  }

}

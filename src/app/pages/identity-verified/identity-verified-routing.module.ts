import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { IdentityVerifiedPage } from './identity-verified.page';

const routes: Routes = [
  {
    path: '',
    component: IdentityVerifiedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class IdentityVerifiedPageRoutingModule {}

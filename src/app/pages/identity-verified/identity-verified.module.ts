import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { IdentityVerifiedPageRoutingModule } from './identity-verified-routing.module';

import { IdentityVerifiedPage } from './identity-verified.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IdentityVerifiedPageRoutingModule,
    FooterModule,
    ReactiveFormsModule,
    FooterModule,
    TranslateModule
  ],
  declarations: [IdentityVerifiedPage, HeaderComponent]
})
export class IdentityVerifiedPageModule {}

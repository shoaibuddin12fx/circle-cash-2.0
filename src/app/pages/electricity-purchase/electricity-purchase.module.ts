import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ElectricityPurchasePageRoutingModule } from './electricity-purchase-routing.module';

import { ElectricityPurchasePage } from './electricity-purchase.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ElectricityPurchasePageRoutingModule,
    ReactiveFormsModule,
    HeaderBarModule,
    FooterModule,
    TranslateModule
  ],
  declarations: [ElectricityPurchasePage]
})
export class ElectricityPurchasePageModule {}

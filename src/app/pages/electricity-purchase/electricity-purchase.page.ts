import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-electricity-purchase',
  templateUrl: './electricity-purchase.page.html',
  styleUrls: ['./electricity-purchase.page.scss'],
})
export class ElectricityPurchasePage extends BasePage implements OnInit {

  submitted = false;
  aForm: FormGroup;
  constructor(injector:Injector) {
    super(injector)
    this.setupForm();
   }

  ngOnInit() {
  }

  setupForm(){
    this.aForm = this.formBuilder.group({
      meterNumber: ['', Validators.compose([Validators.required, Validators.minLength(5) ]) ],
    })
  }

  get meterNumber() {return this.aForm.get("meterNumber")}

  async onSubmit(){
    if(this.aForm.invalid) return;

    var obj = this.aForm.value;
    var userId = await this.sqlite.getActiveUserId();
    obj['userId'] = userId;

    var response = await this.network.addMeterAccount(obj);
    if(response && response.success === true){
      this.utility.presentToast("Success");
      this.nav.pop();
    }
  }
}
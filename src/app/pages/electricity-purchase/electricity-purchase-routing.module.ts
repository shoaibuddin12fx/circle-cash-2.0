import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ElectricityPurchasePage } from './electricity-purchase.page';

const routes: Routes = [
  {
    path: '',
    component: ElectricityPurchasePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ElectricityPurchasePageRoutingModule {}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouteReuseStrategy } from '@angular/router';
import { Camera } from '@ionic-native/camera';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { TranslateModule } from '@ngx-translate/core';
import { PagesRoutingModule } from './pages.routing.module';

@NgModule({
  declarations: [],
  entryComponents: [],
  imports: [CommonModule, IonicModule, PagesRoutingModule,TranslateModule],
//   providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  providers:[
    
  ]
})
export class PagesModule {}

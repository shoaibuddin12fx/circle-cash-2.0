import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LocalCashCircleAccountPageRoutingModule } from './local-cash-circle-account-routing.module';

import { LocalCashCircleAccountPage } from './local-cash-circle-account.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocalCashCircleAccountPageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [LocalCashCircleAccountPage]
})
export class LocalCashCircleAccountPageModule {}

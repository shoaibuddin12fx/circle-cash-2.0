import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocalCashCircleAccountPage } from './local-cash-circle-account.page';

const routes: Routes = [
  {
    path: '',
    component: LocalCashCircleAccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocalCashCircleAccountPageRoutingModule {}

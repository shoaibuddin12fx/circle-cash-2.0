import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-local-cash-circle-account',
  templateUrl: './local-cash-circle-account.page.html',
  styleUrls: ['./local-cash-circle-account.page.scss'],
})
export class LocalCashCircleAccountPage extends BasePage implements OnInit {
  data;
  constructor(injector:Injector) {super(injector)}

  ngOnInit() {
    this.data = Object.assign({}, this.nav.getQueryParams());
  }

async onConfirm(){
  var userId = await this.sqlite.getActiveUserId();
  this.data['userId'] = userId;

  let result = await this.network.addLocalCashCircleAccount(this.data);
  if(result && result.success === true) {
    this.utility.presentToast("Success")
    this.nav.navigateTo('pages/home')
  }
}

}

import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-telecom-services',
  templateUrl: './telecom-services.page.html',
  styleUrls: ['./telecom-services.page.scss'],
})
export class TelecomServicesPage extends BasePage implements OnInit {
  cardSelected = true;
  submitted = false;
  aForm: FormGroup;
  //bForm: FormGroup;
  selectedConnectionType;
  detailVisible = false;
  selectedMobile;
  mobileNumbers = [];
  cards = [];
  cardId: String;
  constructor(injector: Injector) {
    super(injector);
   }

  ngOnInit() {
    this.setupForm();
    this.getMobileNumbers();
  }

  async getMobileNumbers(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getMobileAccounts({userId: userId});
    console.log(result);
    if(result && result.success === true)
      this.mobileNumbers = result.result;
      console.log('mobileNumbers', this.mobileNumbers);
      this.getCards();
  }

  async getCards(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getCards({userId: userId});
    console.log(result);

    if(result && result.success === true){
      this.cards = result.result;
      console.log('cards', this.cards);
    }
  }

  setupForm(){
    this.aForm = this.formBuilder.group({
      mobileId: ['', Validators.compose([Validators.required]) ],
      amount: ['', Validators.compose([Validators.required,Validators.pattern('^[0-9]*$')]) ]
    })

    // this.bForm = this.formBuilder.group({
    //   cardId: ['', Validators.compose([])]
    // })
  }

  get mobileId() {return this.aForm.get("mobileId")}
  get amount() {return this.aForm.get("amount")}
  //get cardId() {return this.bForm.get("cardId")}
  
  async onSubmit() {
    
    if(!this.detailVisible) {
      this.submitted = true;

    if(this.aForm.invalid) { 
      console.log("Invalid");
      return;
    }

    this.selectedMobile = this.mobileNumbers.filter(x=> x._id === this.mobileId.value)[0];
    this.selectedConnectionType = this.selectedMobile.connectionType;
    this.detailVisible = true;
    } else {
      console.log("cardId", this.cardId)
      if(this.cardSelected && !this.cardId) return;
      let mobileNumber = this.mobileNumbers.filter(x=> x._id === this.mobileId.value)[0].mobileNumber;
      var obj = this.aForm.value;
      var userId = await this.sqlite.getActiveUserId();
      obj['userId'] = userId;
      obj['mobileNumber'] = mobileNumber;

      if(this.cardId)
        obj['cardId'] = this.cardId;
      this.cardSelected ? obj["PaymentMethod"] = "Card" : obj["PaymentMethod"] = "Mobile Wallet";
      var response = await this.network.topupMobile(obj);
      if(response.success == true) {
        this.utility.presentToast("Recharged Successfully!");
        this.events.publish('balanceRefresh');
        this.nav.pop();
      }
    }
    
  }
}
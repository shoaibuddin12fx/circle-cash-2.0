import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyQrCodePageRoutingModule } from './my-qr-code-routing.module';

import { MyQrCodePage } from './my-qr-code.page';
import { HeaderComponent } from '../../components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyQrCodePageRoutingModule
  ],
  declarations: [MyQrCodePage, HeaderComponent]
})
export class MyQrCodePageModule {}

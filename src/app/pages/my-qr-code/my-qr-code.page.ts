import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-my-qr-code',
  templateUrl: './my-qr-code.page.html',
  styleUrls: ['./my-qr-code.page.scss'],
})
export class MyQrCodePage extends BasePage implements OnInit {

  user;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.user = this.users.getLocalUser();
    console.log(this.user);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AccountCreatedPageRoutingModule } from './account-created-routing.module';

import { AccountCreatedPage } from './account-created.page';
import { HeaderComponent } from 'src/app/components/header/header.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AccountCreatedPageRoutingModule,
    TranslateModule
  ],
  declarations: [AccountCreatedPage, HeaderComponent]
})
export class AccountCreatedPageModule {}

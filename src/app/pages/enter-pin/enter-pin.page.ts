import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-enter-pin',
  templateUrl: './enter-pin.page.html',
  styleUrls: ['./enter-pin.page.scss'],
})
export class EnterPinPage extends BasePage implements OnInit {
  OTP: string = '';
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  navigate(path) {
    this.nav.navigateTo(path);
  }

  otpController(event, next, prev, index) {
    if (index == 6) {
      console.log('submit');
    }
    if (event.target.value.length < 1 && prev) {
      prev.setFocus();
    } else if (next && event.target.value.length > 0) {
      next.setFocus();
    } else {
      return 0;
    }
    console.log('OTP', this.OTP);
  }

  numberPressed(number) {
    if (this.OTP.length < 4) {
      this.OTP += number;
    }
  }

  numberdeleted() {
    this.OTP = this.OTP.substring(0, this.OTP.length - 1);
  }

  async onConfirm() {
    if (this.OTP && this.OTP.length === 4) {
      let obj = {};
      var userId = await this.sqlite.getActiveUserId();
      obj['userId'] = userId;
      obj['pin'] = this.OTP;
      let result = await this.network.savePin(obj);
      if (result && result.success === true) {
        this.utility.presentToast('PIN generated successfully!');
        this.popToRoot();
      }
    }
  }
  // this.OTP[this.OTP.length -1] = number;
}

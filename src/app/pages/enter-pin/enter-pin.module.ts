import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EnterPinPageRoutingModule } from './enter-pin-routing.module';

import { EnterPinPage } from './enter-pin.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EnterPinPageRoutingModule,
    HeaderBarModule
  ],
  declarations: [EnterPinPage]
})
export class EnterPinPageModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage implements OnInit {

  aForm: FormGroup;
  constructor(injector: Injector, private menu: MenuController) {
    super(injector);
    this.setupForm();
  }

  ngOnInit() {
    this.menu.enable(true,"drawer");
  }

  setupForm(){

    this.aForm = this.formBuilder.group({
      mobileNumber: ['', Validators.compose([Validators.required, ]) ],
      password: ['', Validators.compose([Validators.required, ]) ]

    })
  }

  async login(){

    if(!this.aForm.valid){
      alert("Please insert mobile number & Password");
      return;
    }

    let formdata = this.aForm.value;
    //console.log({formdata})


    let flag = await this.users.login(formdata);

    if (flag == true){
      console.log('Loggedin');
      this.events.publish('login_event');
      // redirect to dasbpoard
    }
    else{
      console.log('Not Loggedin'); 
    } 

  }




  navigate(path){
    this.nav.navigateTo(path)
  }

  onTelephoneChange($event){

    if ($event.inputType !== 'deleteContentBackward') {
      const utel = this.utility.onkeyupFormatPhoneNumberRuntime($event.target.value, false);
      console.log(utel);
      $event.target.value = utel;
      this.aForm.controls['mobileNumber'].patchValue(utel);
    }

  }

}

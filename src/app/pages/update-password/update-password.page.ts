import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-update-password',
  templateUrl: './update-password.page.html',
  styleUrls: ['./update-password.page.scss'],
})
export class UpdatePasswordPage extends BasePage implements OnInit {

  aForm: FormGroup;
  submitted = false;
  constructor(injector:Injector) 
  {
    super(injector);
  }

  ngOnInit() {
    this.setupForm();
  }

  setupForm(){
    this.aForm = this.formBuilder.group({
      oldPassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])], 
      newPassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      confirmPassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    })
  }

  get currentPassword(){return this.aForm.get('oldPassword')}
  get newPassword(){return this.aForm.get('newPassword')}
  get confirmPassword(){return this.aForm.get('confirmPassword')}

  async onSubmit(){
    this.submitted = true;
    if(this.newPassword.value !== this.confirmPassword.value){
      this.utility.presentToast("Passwords do not match")
    } else {
      var userId = await this.sqlite.getActiveUserId();
      let obj = this.aForm.value;
      obj['userId'] = userId;
      let result = await this.network.changePassword(obj)
      
      if(result && result.success === true){
        this.utility.presentToast("Success");
        this.nav.navigateTo('pages/home');
      }

    }

  }

}

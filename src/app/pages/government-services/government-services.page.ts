import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-government-services',
  templateUrl: './government-services.page.html',
  styleUrls: ['./government-services.page.scss'],
})
export class GovernmentServicesPage extends BasePage implements OnInit {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

  navigate(path){
    this.nav.navigateTo(path)
  }

}

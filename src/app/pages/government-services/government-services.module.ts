import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GovernmentServicesPageRoutingModule } from './government-services-routing.module';

import { GovernmentServicesPage } from './government-services.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GovernmentServicesPageRoutingModule,
    HeaderBarModule,
    FooterModule,
    TranslateModule
  ],
  declarations: [GovernmentServicesPage]
})
export class GovernmentServicesPageModule {}

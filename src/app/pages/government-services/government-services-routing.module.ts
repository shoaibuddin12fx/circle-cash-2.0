import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GovernmentServicesPage } from './government-services.page';

const routes: Routes = [
  {
    path: '',
    component: GovernmentServicesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GovernmentServicesPageRoutingModule {}

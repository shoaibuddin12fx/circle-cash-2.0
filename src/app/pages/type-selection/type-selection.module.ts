import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TypeSelectionPageRoutingModule } from './type-selection-routing.module';

import { TypeSelectionPage } from './type-selection.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TypeSelectionPageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [TypeSelectionPage]
})
export class TypeSelectionPageModule {}

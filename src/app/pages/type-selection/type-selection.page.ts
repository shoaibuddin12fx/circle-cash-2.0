import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-type-selection',
  templateUrl: './type-selection.page.html',
  styleUrls: ['./type-selection.page.scss'],
})
export class TypeSelectionPage extends BasePage implements OnInit {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

  navigate(path){
    this.nav.navigateTo(path)
  }

}

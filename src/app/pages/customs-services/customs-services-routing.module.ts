import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomsServicesPage } from './customs-services.page';

const routes: Routes = [
  {
    path: '',
    component: CustomsServicesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomsServicesPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomsServicesPageRoutingModule } from './customs-services-routing.module';

import { CustomsServicesPage } from './customs-services.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomsServicesPageRoutingModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule,
    ReactiveFormsModule
  ],
  declarations: [CustomsServicesPage]
})
export class CustomsServicesPageModule {}

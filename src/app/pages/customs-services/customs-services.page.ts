import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-customs-services',
  templateUrl: './customs-services.page.html',
  styleUrls: ['./customs-services.page.scss'],
})
export class CustomsServicesPage extends BasePage implements OnInit {
  isInquiry: boolean = false;
  cardSelected = true; 
  inquiryForm: FormGroup;
  payForm:FormGroup;
  inquirySubmitted = false;
  paySubmitted = false;
  mobileNumbers = [];
  cards = [];

  constructor(injector:Injector) {super(injector)}

  ngOnInit() {
    this.setupForms();
    this.getMobileNumbers();
  }

  setupForms(){
    this.inquiryForm = this.formBuilder.group({
      invoiceNumber: ['', Validators.compose([Validators.required])],
      mobileId: ['', Validators.compose([Validators.required])],
      cardId: ['', Validators.compose([Validators.required])]
    })

    this.payForm = this.formBuilder.group({
      bankCode: ['', Validators.compose([Validators.required])],
      delcarentCode: ['', Validators.compose([Validators.required])],
      amount: ['', Validators.compose([Validators.required,Validators.pattern('^[0-9]*$')])],
      cardId: ['', Validators.compose([])]
    })
  }

  get inquiryInvoiceNumber() {return this.inquiryForm.get('invoiceNumber')}
  get inquiryMobileNumber() {return this.inquiryForm.get('mobileId')}
  get inquiryCardNumber() {return this.inquiryForm.get('cardId')}

  get payBankCode() {return this.payForm.get('bankCode')} 
  get payDelcarentCode() {return this.payForm.get('delcarentCode')}
  get payAmount() {return this.payForm.get('amount')}
  get payCardNumber() {return this.payForm.get('cardId')} 
 
  async getMobileNumbers(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getMobileAccounts({userId: userId});
    console.log(result);

    if(result && result.success === true){
      this.mobileNumbers = result.result;
      console.log('mobileNumbers', this.mobileNumbers);
      this.getCards();
    }
  }

  async getCards(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getCards({userId: userId});
    console.log(result);

    if(result && result.success === true){
      this.cards = result.result;
      console.log('cards', this.cards);
    }
  }

  async onSubmit(){
    if(this.isInquiry){
      this.inquirySubmitted = true;
        if(this.inquiryForm.invalid) return;
        
        var userId = await this.sqlite.getActiveUserId();
        let obj = this.inquiryForm.value;
        obj['userId'] = userId;

        let result = await this.network.inquiryCustomsServices(obj);
        if(result && result.success === true){
          this.utility.presentToast("Inquiry received");
          this.popToRoot();
        }

    } else {
      this.paySubmitted = true;
        if(this.payForm.invalid) return;
        if(this.cardSelected && !this.payCardNumber.value) return;
        var userId = await this.sqlite.getActiveUserId();
        let obj = this.payForm.value;
       if(this.cardSelected){
         let cardNumber = this.cards.filter(x=> x._id === this.payCardNumber.value)[0].cardNumber;
         obj['cardNumber'] = cardNumber;
       }
        obj['userId'] = userId;
        obj['PaymentMethod'] = this.cardSelected ? 'Card' : 'Mobile Wallet';

        let result = await this.network.payCustomsServices(obj);
        if(result && result.success === true) {
          this.utility.presentToast("Success");
          this.events.publish('balanceRefresh');          
          this.popToRoot();
        }
    }
  }

  popToRoot(){
    this.nav.navigateTo('pages/home')
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CardTransferListPageRoutingModule } from './card-transfer-list-routing.module';

import { CardTransferListPage } from './card-transfer-list.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CardTransferListPageRoutingModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [CardTransferListPage]
})
export class CardTransferListPageModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CardTransferListPage } from './card-transfer-list.page';

const routes: Routes = [
  {
    path: '',
    component: CardTransferListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CardTransferListPageRoutingModule {}

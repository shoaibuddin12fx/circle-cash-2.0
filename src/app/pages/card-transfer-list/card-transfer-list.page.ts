import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-card-transfer-list',
  templateUrl: './card-transfer-list.page.html',
  styleUrls: ['./card-transfer-list.page.scss'],
})
export class CardTransferListPage extends BasePage implements OnInit {
  dataList:any = []

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.dataList = [
      {id:1},
      {id:2},
      {id:3},
      {id:4},
    ]
  }

  navigate(path){
    this.nav.navigateTo(path)
  }

}

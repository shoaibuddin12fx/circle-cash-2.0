import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { CardTransferPage } from './card-transfer.page';

const routes: Routes = [
  {
    path: '',
    component: CardTransferPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes),
  FormsModule,
  ReactiveFormsModule,
  TranslateModule,

  ],
  exports: [RouterModule],

})
export class CardTransferPageRoutingModule {}

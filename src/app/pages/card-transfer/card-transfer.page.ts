import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-card-transfer',
  templateUrl: './card-transfer.page.html',
  styleUrls: ['./card-transfer.page.scss'],
})
export class CardTransferPage extends BasePage implements OnInit {
  aForm: FormGroup;
  submitted = false;
  cards = [];
  bankAccounts = [];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.setupform();
    this.getCards();
  }

setupform(){
  this.aForm = this.formBuilder.group({
    cardId:['', Validators.compose([Validators.required])],
    accountId:['',Validators.compose([Validators.required])],
    amount:['',Validators.compose([Validators.required,Validators.pattern('^[0-9]*$')])]
  });
}

get transfer(){return this.aForm.get('cardId')};
get benificary() {return this.aForm.get('accountId')};
get Amount() {return this.aForm.get('amount')};

  navigate(path){
    this.nav.navigateTo(path)
  }

  async getCards(){
    var userId = await this.sqlite.getActiveUserId();
    let obj = {userId : userId}
    let result = await this.network.getCards(obj);
    
    if(result && result.success === true){
      this.cards = result.result;
      console.log(this.cards)
      this.getAccounts();
    }
   // this.utility.hideLoader();
  }

  async getAccounts(){
    var userId = await this.sqlite.getActiveUserId();
    let obj = {userId : userId}
    let result = await this.network.getBankAccounts(obj);
    if(result && result.success === true){
      this.bankAccounts = result.result;
      console.log(this.bankAccounts)

    } 
    //this.utility.hideLoader();
      
  }

  async onSubmit() { 
    this.submitted = true;
    if(this.aForm.invalid) return;

    var obj = this.aForm.value;
    console.log("obj", obj);

    var userId = await this.sqlite.getActiveUserId();
    let cardData = this.cards.filter(x => x._id === obj.cardId)[0];
    console.log(cardData);
    let bankData = this.bankAccounts.filter(x => x._id === this.benificary.value)[0];
    obj['cardNumber'] = cardData.cardNumber;
    obj['beneficary'] = bankData.nickName;
    obj['userId'] = userId;
    this.nav.push('pages/card-transfer-details',obj);
  }

}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ScanQrCodePageRoutingModule } from './scan-qr-code-routing.module';

import { ScanQrCodePage } from './scan-qr-code.page';
import { HeaderComponent } from '../../components/header/header.component';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ScanQrCodePageRoutingModule,
    FooterModule
  ],
  declarations: [ScanQrCodePage, HeaderComponent]
})
export class ScanQrCodePageModule {}

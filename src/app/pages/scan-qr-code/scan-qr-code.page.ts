import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';


@Component({
  selector: 'app-scan-qr-code',
  templateUrl: './scan-qr-code.page.html',
  styleUrls: ['./scan-qr-code.page.scss'],
})
export class ScanQrCodePage extends BasePage implements OnInit {

  constructor(injector: Injector, private barcodeScanner: BarcodeScanner) {
    super(injector);
  }

  ngOnInit() {

  }

  navigateToHome(){
    this.nav.navigateTo('pages/home');
  }

  qrScan(){
    this.getBarcodeResult();
    
    this.barcodeScanner.scan().then(barcodeData => {
      console.log('Barcode data', barcodeData);

      let value = barcodeData.text;

      // call API here
     }).catch(err => {
         console.log('Error', err);
     });
  }

 async getBarcodeResult(){
    var userId = await this.sqlite.getActiveUserId();
    let obj = {};
    obj["userId"] = userId;
    obj["iban"] = "123456"
    let result = await this.network.getQRCodeResult(obj);
    if(result && result.success === true){
      //TODO
    }
  }

}

import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-otp-verify',
  templateUrl: './otp-verify.page.html',
  styleUrls: ['./otp-verify.page.scss'],
})
export class OtpVerifyPage extends BasePage implements OnInit {

  OTP: string = '';
  data;
  constructor(injector: Injector) {
    super(injector);
    let params = this.nav.getQueryParams();
    this.data = Object.assign({},params);
    this.OTP = params.otp;
  }

  ngOnInit() {
  }

  otpController(event,next,prev, index){

    if(index == 6) {
      console.log("submit")
    }
    if(event.target.value.length < 1 && prev){
      prev.setFocus()
    }
    else if(next && event.target.value.length>0){
      next.setFocus();
    }
    else {
     return 0;
    }
    console.log("OTP" ,this.OTP) 
 }

 async onResend(){
    
    let userId = await this.sqlite.getActiveUserId();
    this.data['userId'] = userId;
    let result = await this.network.resendOTP(this.data);
    
    if(result && result.success === true){
      console.log(result);
      this.OTP = result.result.toString();
      console.log(this.OTP)
      this.utility.presentToast("OTP Sent Success");
      this.data.otp = result.result;
    }
      
  }


  async verifyOTP(){
    let result = await this.network.verifyOtp(this.data);
    if(result && result.success === true){
      this.nav.push('pages/enter-pin')
      console.log(result);
    }
      
  }

}

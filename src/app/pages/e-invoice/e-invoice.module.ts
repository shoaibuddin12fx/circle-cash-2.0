import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EInvoicePageRoutingModule } from './e-invoice-routing.module';

import { EInvoicePage } from './e-invoice.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EInvoicePageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [EInvoicePage]
})
export class EInvoicePageModule {}

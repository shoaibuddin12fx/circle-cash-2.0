import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';


@Component({
  selector: 'app-e-invoice',
  templateUrl: './e-invoice.page.html',
  styleUrls: ['./e-invoice.page.scss'],
})
export class EInvoicePage extends BasePage implements OnInit {
  dataList: any = [];
  @Input() item;

  constructor(injector:Injector) {
    super(injector);
  }

  ionViewWillEnter(){
    const temp = JSON.parse( localStorage.getItem('selectedInvoice') );
    if(!temp){
      this.nav.pop()
    }

    this.item = temp;

  }

  ngOnInit() {
    this.getTransactions();
  }

  navigate(data){
    this.nav.push('pages/e-invoice',data)
  }

  async getTransactions(){
    var userId = await this.sqlite.getActiveUserId();
    let result = await this.network.transactionHistoryOfCustomer('?userId='+userId);
    if(result && result.success === true){
      let transactions = result.result;
      console.log(transactions);
      const userId = await this.sqlite.getActiveUserId();
      transactions.forEach(x => {
        if(x.sender_id === userId)
          x["isSender"] = true;
        else x["isSender"] = false;
      });
      this.dataList = transactions;
    }
  }
}

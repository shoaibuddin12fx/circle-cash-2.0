import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EInvoicePage } from './e-invoice.page';

const routes: Routes = [
  {
    path: '',
    component: EInvoicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EInvoicePageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GenerateVoucherPageRoutingModule } from './generate-voucher-routing.module';

import { GenerateVoucherPage } from './generate-voucher.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GenerateVoucherPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [GenerateVoucherPage]
})
export class GenerateVoucherPageModule {}

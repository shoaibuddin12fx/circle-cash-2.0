import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-generate-voucher',
  templateUrl: './generate-voucher.page.html',
  styleUrls: ['./generate-voucher.page.scss'],
})
export class GenerateVoucherPage extends BasePage implements OnInit {
  aForm: FormGroup;
  mobileAccounts = [];
  cards = [];
  submitted = false;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.setupForm();
    this.getMobileAccounts();
  }

  setupForm(){
    this.aForm = this.formBuilder.group({
      mobileId: ['', Validators.compose([Validators.required ]) ],
      cardId: ['', Validators.compose([Validators.required])],
      amount: ['', Validators.compose([Validators.required,Validators.pattern('^[0-9]*$')]) ]
    })
  }

  get mobileId() {return this.aForm.get("mobileId")}
  get cardId() {return this.aForm.get("cardId")}
  get amount() {return this.aForm.get("amount")}

  navigate(path){
    this.nav.navigateTo(path)
  }

  async getMobileAccounts(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getMobileAccounts({userId: userId});
    console.log(result);
    if(result && result.success === true){
      this.mobileAccounts = result.result;
      this.getCards()
    }
  }

  async getCards(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getCards({userId: userId});
    console.log(result);
    if(result && result.success === true){
      this.cards = result.result;
    }
  }

  async onSubmit(){
    this.submitted = true;
    if(this.aForm.invalid) return;
    var obj = this.aForm.value;
    var userId = await this.sqlite.getActiveUserId();
    let mobileObj = this.mobileAccounts.filter(x => x._id === obj.mobileId)[0];
    let cardObj = this.cards.filter(x => x._id === obj.cardId)[0];
    obj['userId'] = userId;
    obj['cardNumber'] = cardObj.cardNumber;
    obj['mobileNumber'] = mobileObj.mobileNumber;
    this.nav.push('pages/generate-voucher-details',obj)
  }

}

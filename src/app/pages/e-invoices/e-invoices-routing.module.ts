import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EInvoicesPage } from './e-invoices.page';

const routes: Routes = [
  {
    path: '',
    component: EInvoicesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EInvoicesPageRoutingModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { EInvoicePage } from '../e-invoice/e-invoice.page';

@Component({
  selector: 'app-e-invoices',
  templateUrl: './e-invoices.page.html',
  styleUrls: ['./e-invoices.page.scss'],
})
export class EInvoicesPage extends BasePage implements OnInit {
dataList:any = [];

constructor(injector: Injector) {
  super(injector);
}

  ngOnInit() {
    this.getTransactions();
  }

  navigate(data){
    this.nav.push('pages/e-invoice',data)
  }

  openDetails(item){

    localStorage.setItem('selectedInvoice', JSON.stringify(item));
    this.nav.push('pages/e-invoice')


  }

  async getTransactions(){
    var userId = await this.sqlite.getActiveUserId();
    let result = await this.network.transactionHistoryOfCustomer('?userId='+userId);
    if(result && result.success === true){
      let transactions = result.result;
      console.log(transactions);
      const userId = await this.sqlite.getActiveUserId();
      transactions.forEach(x => {
        if(x.sender_id === userId)
          x["isSender"] = true;
        else x["isSender"] = false;
      });
      this.dataList = transactions;
    }
  }

}

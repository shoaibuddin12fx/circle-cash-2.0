import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EInvoicesPageRoutingModule } from './e-invoices-routing.module';

import { EInvoicesPage } from './e-invoices.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EInvoicesPageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [EInvoicesPage]
})
export class EInvoicesPageModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PaymentsFormCardPageRoutingModule } from './payments-form-card-routing.module';

import { PaymentsFormCardPage } from './payments-form-card.page';
import { HeaderComponent } from '../../components/header/header.component';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PaymentsFormCardPageRoutingModule,
    FooterModule
  ],
  declarations: [PaymentsFormCardPage, HeaderComponent]
})
export class PaymentsFormCardPageModule {}

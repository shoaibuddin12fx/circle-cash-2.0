import { Component, Injector, OnInit } from '@angular/core';
import { PaymentSuccessModalComponent } from 'src/app/components/payment-success-modal/payment-success-modal.component';
import { BasePage } from '../base-page/base-page';

declare var $:any;
@Component({
  selector: 'app-payments-form-card',
  templateUrl: './payments-form-card.page.html',
  styleUrls: ['./payments-form-card.page.scss'],
})
export class PaymentsFormCardPage extends BasePage implements OnInit {
  
  public cards: any;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.cards = [
      {
        state: 'ON',
        logo: "assets/icon/visa.png",
        a: 1234,
        b: 5522,
        c: 8432,
        d: 2264,
        expires: '7/12',
        bank: 'Bank of America'
      },
      {
        state: 'OFF',
        logo: "assets/icon/american.png",
        a: 1234,
        b: 5321,
        c: 8283,
        d: 9271,
        expires: '8/19',
        bank: 'JPMorgan'
      },
      {
        state: 'ON',
        logo: "assets/icon/mastercard.png",
        a: 8685,
        b: 2445,
        c: 9143,
        d: 7846,
        expires: '11/23',
        bank: 'CityBank'
      }
    ];
  } 

  // ------------------------ Payment Success Modal ------------------------------ //

 ShowPaymentSuccessModal() {
   this.modals.present(PaymentSuccessModalComponent);
  //  console.log('Show...');
  //   $('#paymentSuccessModal').modal('show')
   }

  HidePaymentSuccessModal() {
    $('#paymentSuccessModal').modal('hide')
  }

}

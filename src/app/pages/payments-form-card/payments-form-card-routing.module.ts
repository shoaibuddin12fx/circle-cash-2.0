import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PaymentsFormCardPage } from './payments-form-card.page';

const routes: Routes = [
  {
    path: '',
    component: PaymentsFormCardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PaymentsFormCardPageRoutingModule {}

import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-payee-list',
  templateUrl: './payee-list.page.html',
  styleUrls: ['./payee-list.page.scss'],
})
export class PayeeListPage extends BasePage implements OnInit {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

  navigate(path){
    this.nav.navigateTo(path)
  }

}

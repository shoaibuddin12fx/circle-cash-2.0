import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PayeeListPageRoutingModule } from './payee-list-routing.module';

import { PayeeListPage } from './payee-list.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PayeeListPageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [PayeeListPage]
})
export class PayeeListPageModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLocalBankAccountPageRoutingModule } from './add-local-bank-account-routing.module';

import { AddLocalBankAccountPage } from './add-local-bank-account.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddLocalBankAccountPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [AddLocalBankAccountPage]
})
export class AddLocalBankAccountPageModule {}

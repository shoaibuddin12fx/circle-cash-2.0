import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-add-local-bank-account',
  templateUrl: './add-local-bank-account.page.html',
  styleUrls: ['./add-local-bank-account.page.scss'],
})
export class AddLocalBankAccountPage extends BasePage implements OnInit {

  aForm :FormGroup;
  submitted = false;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.setupform()
  }
  setupform() {
    this.aForm = this.formBuilder.group({
      bankName:['', Validators.compose([Validators.required])],
      accountNumber:['',Validators.compose([Validators.required, Validators.minLength(11), Validators.maxLength(50)])],
      nickName:['', Validators.compose([Validators.required])],
    });
  }

  get bankName () { return this.aForm.get('bankName')};
  get accountNumber () {return this.aForm.get('accountNumber')};
  get nickName () {return this.aForm.get('nickName')};
  
  navigate(path){
    this.nav.navigateTo(path)
  }

  async onSubmit(){
    this.submitted = true;
    if(this.aForm.invalid) return;

    var obj = this.aForm.value;
    var userId = await this.sqlite.getActiveUserId();
    obj['userId'] = userId;
   // let bankFullName = banks
    this.nav.push("pages/add-local-bank-account-confirmation", obj);

    
  }

}

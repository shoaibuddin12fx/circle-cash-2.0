import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddLocalBankAccountPage } from './add-local-bank-account.page';

const routes: Routes = [
  {
    path: '',
    component: AddLocalBankAccountPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddLocalBankAccountPageRoutingModule {}

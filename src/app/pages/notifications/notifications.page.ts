import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage extends BasePage implements OnInit {

  dataList:any = []
  constructor(injector:Injector) {super(injector)}

  ngOnInit() {
    this.getNotifications();
  }

  async getNotifications(){
    var userId = await this.sqlite.getActiveUserId();
    let result = await this.network.transactionHistoryOfCustomer('?userId='+userId);
    if(result && result.success === true){
      let notifications = result.result;
      console.log(notifications);
      const userId = await this.sqlite.getActiveUserId();
      notifications.forEach(x => {
        if(x.sender_id === userId)
          x["isSender"] = true;
        else x["isSender"] = false;
      });
      this.dataList = notifications;
    }
  }

}

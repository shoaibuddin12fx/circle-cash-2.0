import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-delete-card',
  templateUrl: './delete-card.page.html',
  styleUrls: ['./delete-card.page.scss'],
})
export class DeleteCardPage extends BasePage implements OnInit {
  obj;
  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    let params = this.nav.getQueryParams();
    this.obj = Object.assign({},params)
  }

  async onDelete() {
    var userId = await this.sqlite.getActiveUserId();
    this.obj["userId"] = userId;
    this.obj["cardId"] = this.obj._id;
    let result = await this.network.deleteCard(this.obj);
    if(result && result.success === true){
      this.utility.presentToast("Success");
      this.nav.pop();
    }
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeleteCardPageRoutingModule } from './delete-card-routing.module';

import { DeleteCardPage } from './delete-card.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeleteCardPageRoutingModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [DeleteCardPage]
})
export class DeleteCardPageModule {}

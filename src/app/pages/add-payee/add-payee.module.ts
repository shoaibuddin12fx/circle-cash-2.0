import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPayeePageRoutingModule } from './add-payee-routing.module';

import { AddPayeePage } from './add-payee.page';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddPayeePageRoutingModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [AddPayeePage]
})
export class AddPayeePageModule {}

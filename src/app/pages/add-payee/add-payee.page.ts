import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-add-payee',
  templateUrl: './add-payee.page.html',
  styleUrls: ['./add-payee.page.scss'],
})
export class AddPayeePage extends BasePage implements OnInit {

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
  }

  navigate(path){
    this.nav.navigateTo(path)
  }
}

import { Component, Injector, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-education-bills-and-fees',
  templateUrl: './education-bills-and-fees.page.html',
  styleUrls: ['./education-bills-and-fees.page.scss'],
})
export class EducationBillsAndFeesPage extends BasePage implements OnInit {
  cardSelected = true;
  aForm: FormGroup;
  submitted = false;
  cards: any;
  constructor(injector:Injector) 
  {
    super(injector);
    this.setupForm();
    this.getCards();
   }

  ngOnInit() {
  }

  get cardNumber() {return this.aForm.get('cardId')}
  get studentType() {return this.aForm.get('studentType')}
  get admissionType() {return this.aForm.get('admissionType')}
  get courseType() {return this.aForm.get('courseType')}
  get seatNumber() {return this.aForm.get('seatNumber')}
  get amount() {return this.aForm.get('amount')}


  async getCards(){
    var userId = await this.sqlite.getActiveUserId();
    console.log({userId})
    let result = await this.network.getCards({userId: userId});
    console.log(result);

    if(result && result.success === true){
      this.cards = result.result;
      console.log('cards', this.cards);
    }
  }

  setupForm(){
    this.aForm = this.formBuilder.group({
      cardId:['', Validators.compose([])],
      studentType: ['', Validators.compose([Validators.required]) ],
      admissionType: ['', Validators.compose([Validators.required]) ],
      courseType:['', Validators.compose([Validators.required])],
      seatNumber:['', Validators.compose([Validators.required])],
      amount:['', Validators.compose([Validators.required, Validators.pattern('^[0-9]*$')])]
    })
  }


   async onSubmit(){
    this.submitted = true;
    if(this.aForm.invalid) return;
    var obj = this.aForm.value;
    var userId = await this.sqlite.getActiveUserId();
    obj['userId'] = userId;
    if(this.cardSelected && !this.cardNumber.value) return;
    if(this.cardSelected){
      obj["cardNumber"] = this.cards.filter(x=> x._id === this.cardNumber.value)[0].cardNumber;
    }
    this.cardSelected ? obj["PaymentFrom"] = "Card" : "Mobile Wallet"
    var result = await this.network.addEducationPayment(obj);
    if(result && result.success === true){
      this.utility.presentToast("Success")
      this.events.publish('balanceRefresh');
      this.nav.pop()
    } 
    // else this.utility.presentToast("Error");

  }
}
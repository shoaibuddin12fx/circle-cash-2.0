import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EducationBillsAndFeesPage } from './education-bills-and-fees.page';

const routes: Routes = [
  {
    path: '',
    component: EducationBillsAndFeesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EducationBillsAndFeesPageRoutingModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EducationBillsAndFeesPageRoutingModule } from './education-bills-and-fees-routing.module';

import { EducationBillsAndFeesPage } from './education-bills-and-fees.page';
import { TranslateModule } from '@ngx-translate/core';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { FooterModule } from 'src/app/components/footer/footer.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EducationBillsAndFeesPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    HeaderBarModule,
    FooterModule
  ],
  declarations: [EducationBillsAndFeesPage]
})
export class EducationBillsAndFeesPageModule {}

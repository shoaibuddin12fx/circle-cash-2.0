import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EducationBillsAndFeesPage } from './education-bills-and-fees.page';

describe('EducationBillsAndFeesPage', () => {
  let component: EducationBillsAndFeesPage;
  let fixture: ComponentFixture<EducationBillsAndFeesPage>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EducationBillsAndFeesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EducationBillsAndFeesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomePageRoutingModule } from './home-routing.module';

import { HomePage } from './home.page';

import { FooterModule } from 'src/app/components/footer/footer.module';
import { HeaderBarModule } from 'src/app/components/header-bar/header-bar.component.module';
import { TranslateModule } from '@ngx-translate/core';
import { AES256 } from '@ionic-native/aes-256/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    FooterModule,
    HeaderBarModule,
    TranslateModule,
  ],
  declarations: [HomePage],
})
export class HomePageModule {}

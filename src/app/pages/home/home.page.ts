import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { AES256 } from '@ionic-native/aes-256/ngx';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage extends BasePage implements OnInit {
  private readonly secureKey: string = 'vOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3';
  private secureIV: string;

  constructor(injector: Injector, private aes256: AES256) {
    super(injector);
  }

  ngOnInit() {
    this.generateSecureKeyAndIV();
    this.getTestData();
  }

  navigate(path) {
    this.nav.navigateTo(path);
  }

  async getTestData() {
    let result = await this.network.testCall(null);
    console.log(result.result);
    this.encrypt('TEST12345');
    // this.decrypt(result.result);
  }

  async generateSecureKeyAndIV() {
    // this.secureKey = await this.aes256.generateSecureKey(
    //   'random password 12345'
    // ); // Returns a 32 bytes string
    this.secureIV = await this.aes256.generateSecureIV('CIRCLE_CASH_12345'); // Returns a 16 bytes string
  }

  encrypt(data) {
    console.log('IV', this.secureIV);
    this.aes256
      .encrypt(this.secureKey, this.secureIV, data)
      .then((res) => console.log('Encrypted Data: ', res))
      .catch((error: any) => console.error(error));
  }

  decrypt(hash) {
    console.log(hash.iv, hash.content);
    this.aes256
      .decrypt(this.secureKey, hash.iv, hash.content)
      .then((res) => console.log('Decrypted Data : ', res))
      .catch((error: any) => console.error(error));
  }
}

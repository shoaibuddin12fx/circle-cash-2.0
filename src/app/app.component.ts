import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController, Platform } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { BasePage } from './pages/base-page/base-page';
import { NavService } from './services/nav.service';
import { UtilityService } from './services/utility.service';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent extends BasePage {
  
  isModalOpen;
  user;
  public appPages = [
    { title: 'Account Details', url: '/pages/user-profile', icon: '../../../assets/images/account-details.png' },
    { title: 'My QR Code', url: '/pages/my-qr-code', icon: '../../../assets/images/my-qr-code.png' },
    { title: 'Transaction History', url: '/pages/notifications', icon: '../../../assets/images/transaction-history.png' },
    { title: 'E-Invoices', url: '/pages/e-invoices', icon: '../../../assets/images/e-invoices.png' },
    { title: 'Refferal Program', url: '/folder/Trash', icon: '../../../assets/images/refferal-program.png' },
    { title: 'Help & Support', url: '/folder/Spam', icon: '../../../assets/images/help-support.png' },
    { title: 'Logout', url: '/pages/login', icon: '../../../assets/images/logout.png' },
  ];

  constructor(
    public platform: Platform,
    public nav: NavService,
    public router: Router,
    public utility: UtilityService,
    private modalController: ModalController,
    translate: TranslateService,
    injector:Injector) {
      super(injector)
      this.events.subscribe('login_event',()=>{
        this.user = this.users.getLocalUser();
      })

    // this language will be used as a fallback when a translation isn't found in the current language
    translate.setDefaultLang('en');

    // the lang to use, if the lang isn't available, it will use the current loader to get them
   translate.use('en');
    this.platform.backButton.subscribeWithPriority(1, () => {
      console.log('hit back btn');
    });

    this.router.events.subscribe(async () => {

      // if (router.url.toString() === "/tabs/home" && isModalOpened) this.modalController.dismiss();
    });

    document.addEventListener('backbutton', (event) => {
      event.preventDefault();
      event.stopPropagation();
      const url = this.router.url;
      console.log(url);
      this.createBackRoutingLogics(url);

    }, false);

  }

  async createBackRoutingLogics(url){

    if(url.includes('login')
      || url.includes('signup')
      || url.includes('home')
      || url.includes('splash')) {

        this.utility.hideLoader();

        const isModalOpen = await this.modalController.getTop();
        console.log(isModalOpen);
        if(isModalOpen){
          this.modalController.dismiss({data: 'A'});
        }else{
          this.exitApp();
        }

    }else{
      if(this.isModalOpen){

      }
    }

  }

  exitApp() {
    navigator['app'].exitApp();
  }

  ngOnInit() {
    this.user = this.users.getLocalUser();
    console.log(this.user);
    //this.routerOutlet.swipeGesture = false;
  }

  navigate(path){
    // this.nav.navigateTo(path)
  }

  handleClicked(title){
    if(title === 'Logout'){
      console.log('onLogout')
      this.users.logout();
    }

  }

}

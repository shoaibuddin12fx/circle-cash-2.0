import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent extends BasePage implements OnInit {

  @Input() backBtnHidden = true;
  @Input() notificationHidden = true;
  constructor(injector:Injector) {
    super(injector);
   }

  ngOnInit() {}

  back(){
    console.log('popping back')
    this.nav.pop()
  }

  navigate(path){
    this.nav.navigateTo(path)
  }

}
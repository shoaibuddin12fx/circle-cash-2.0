import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule, IonicRouteStrategy, IonMenu } from '@ionic/angular';


@NgModule({
    imports:[
      CommonModule,
      FormsModule,
      IonicModule
    ],
declarations:[]
})

export class SideMenuModule {}
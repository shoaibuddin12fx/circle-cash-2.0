import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { FooterComponent } from './footer.component';

// import { ForgotPasswordPageRoutingModule } from './forgot-password-routing.module';

// import { ForgotPasswordPage } from './forgot-password.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
  ],
  exports: [FooterComponent],
  declarations: [FooterComponent]
})
export class FooterModule {}
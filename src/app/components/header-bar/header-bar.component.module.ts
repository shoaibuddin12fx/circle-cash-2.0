import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { HeaderBarComponent } from './header-bar.component';

// import { ForgotPasswordPageRoutingModule } from './forgot-password-routing.module';

// import { ForgotPasswordPage } from './forgot-password.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
//    ForgotPasswordPageRoutingModule
  ],
  exports: [HeaderBarComponent],
  declarations: [HeaderBarComponent],
})
export class HeaderBarModule {}
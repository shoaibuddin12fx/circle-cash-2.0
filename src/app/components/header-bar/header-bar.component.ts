import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-header-bar',
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss'],
})
export class HeaderBarComponent extends BasePage implements OnInit {

  @Input() hideBalance = true;
  @Input() hideUserImg = true; 
  @Input() hideNotification = true;
  @Input() hideMore = true;
  @Input() hideBackBtn = true;
  static instances = [];
  user;
  balanceSDG;
  self;
  
  constructor(injector: Injector) {
    super(injector);
    HeaderBarComponent.instances.push(this);
    this.self = this;
    this.events.subscribe('balanceRefresh', this.bindProfile.bind(this) )
  }

  bindProfile(){
    this.getProfile();
  }

  ngOnInit() {
    this.user = this.users.getLocalUser();    
    this.balanceSDG = this.user.amountCDF;
    console.log(this.user);
  }

  navigate(path){
    this.nav.navigateTo(path) 
  }

  back(){
    this.nav.pop();
  }

  openMenu($event){
    this.menuCtrl.open()
  }

  async getProfile(){
    var userId = await this.sqlite.getActiveUserId();
    let result = await this.network.getProfile('?userId='+userId);
    if(result && result.success === true){
      
      let user = result.result;
      console.log(user.amountCDF);
      this.self.users.setLocalUser(user);
      this.balanceSDG = user.amountCDF;
      HeaderBarComponent.instances.forEach(x => {
          x.balanceSDG = user.amountCDF;
          x.user = user;
      });
      
      //this.user = user;
    }

    // let data = await this.getProfileData();
    console.log(result);
  }

}
